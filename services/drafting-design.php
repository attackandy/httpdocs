<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            include("$root/php/html_templates/template.php");
            
            $headContentTemplate = new Template("$root/php/html_templates/head_content.php");
            $headContentTemplate->title = "Drafting & Design";
            echo $headContentTemplate;
        ?>
    </head>
    <body id="body-drafting">
		<?php include_once("analyticstracking.php") ?>
        
        <?php
            $loadingScreenTemplate = new Template("$root/php/html_templates/loading_screen.php");
            echo $loadingScreenTemplate;
        ?>
        
        <?php
            $headerTemplate = new Template("$root/php/html_templates/header.php");
            $headerTemplate->draftingDesignActive = true;
            echo $headerTemplate;
        ?>
        
        <div id="content" class="container-fluid">
            
            <?php
                $brochureRibbonTemplate = new Template("$root/php/html_templates/brochure_ribbon.php");
                echo $brochureRibbonTemplate;
            ?>
            
            <section id="products-header-wrapper" class="pitch-wrapper flexbox-center-vertically">
                <div id="products-header" class="pitch">
                    <div class="row row-top-buffer-small row-bottom-buffer-small">
                        <div class="col-xs-12 relative">
                            <img class="pitch-grisley-g" alt="Grisley" src="/img/grisley_g.png">
                            <div class="pitch-tagline-wrapper center-absolute">
                                <h1 class="pitch-tagline">Drafting & Design</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        
            <section id="products-section">
                <div class="row row-top-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6">
                        <img alt="Drafting & Design 1" src="/img/drafting-design-1.png" class="product-image img-responsive">
                    </div>
                    <div class="col-lg-5 col-sm-6">
                        <h2 class="product-name">In-House Drafting</h2>
                        <p class="product-description"><strong>Grisley In-House Drafting</strong> – Air-Supported Conveying systems require high levels of geometric accuracy and communication to ensure performance. With the use of Solidworks 3D modeling Grisley ASC can efficiently produce high tolerance 2D submittals to help ensure that the shop detailing and fabrication phases are producing the highest quality parts possible.</p>
                        <img alt="Drafting & Design 2" src="/img/drafting-design-2.png" class="img-responsive product-additional-graphic">
                    </div>
                </div>
                <div class="row row-top-buffer">
                    <hr class="col-lg-offset-1 col-lg-10">
                </div>
                <div class="row row-top-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6 col-lg-push-5 col-sm-push-6">
                        <img alt="Drafting & Design 3" src="/img/drafting-design-3.png" class="product-image img-responsive">
                    </div>
                    <div class="col-lg-5 col-sm-6 col-lg-pull-5 col-sm-pull-6">
                        <h2 class="product-name">In-House Drafting</h2>
                        <p class="product-description"><strong>Grisley In-House Drafting</strong> – Conveying systems require high levels of geometric accuracy and communication from Layout through Detail Design to ensure that installation and start-up is a smooth process. Grisley ASC's in-house drafting integrates the high precision of 3D modeled linework with 2D layouts to provide all of the  product information needed to complete projects timely and accurately.</p>
                        <img alt="Drafting & Design 4" src="/img/drafting-design-4.png" class="img-responsive product-additional-graphic">
                    </div>
                </div>
                <div class="row row-top-buffer">
                    <hr class="col-lg-offset-1 col-lg-10">
                </div>
                <div class="row row-top-buffer row-bottom-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6">
                        <img alt="3D Design 1" src="/img/3d-design-1.jpg" class="product-image img-responsive">
                    </div>
                    <div class="col-lg-5 col-sm-6">
                        <h2 class="product-name">3D Design</h2>
                        <p class="product-description"><strong>Grisley 3D Modeling</strong> – Using the proper 3D modeling tools is essential in todays design world. Grisley ASC doesn't just produce a pretty picture. Our 3D modeling software is put to use by creating standard 3D models of all products. With 3D design software Grisley ASC can efficiently and effectively innovate to the customers specific bulk material handling needs.</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <img alt="3D Design 2" src="/img/3d-design-2.jpg" class="img-responsive product-additional-graphic">
                    </div>
                </div>
            </section>
                
        </div>
        
        <?php
            $footerTemplate = new Template("$root/php/html_templates/footer.php");
            echo $footerTemplate;
        ?>
    </body>
</html>
