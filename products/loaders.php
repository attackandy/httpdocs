<!DOCTYPE html>
<html>
    <head>
        <?php
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            include("$root/php/html_templates/template.php");
            
            $headContentTemplate = new Template("$root/php/html_templates/head_content.php");
            $headContentTemplate->title = "Products - Loaders";
            echo $headContentTemplate;
        ?>
    </head>
    <body id="body-products">
		<?php include_once("analyticstracking.php") ?>
        
        <?php
            $loadingScreenTemplate = new Template("$root/php/html_templates/loading_screen.php");
            echo $loadingScreenTemplate;
        ?>
        
        <?php
            $headerTemplate = new Template("$root/php/html_templates/header.php");
            $headerTemplate->loadersActive = true;
            echo $headerTemplate;
        ?>
        
        <div id="content" class="container-fluid">
            
            <?php
                $brochureRibbonTemplate = new Template("$root/php/html_templates/brochure_ribbon.php");
                echo $brochureRibbonTemplate;
            ?>

            <section id="products-header-wrapper" class="pitch-wrapper flexbox-center-vertically">
                <div id="products-header" class="pitch">
                    <div class="row row-top-buffer-small row-bottom-buffer-small">
                        <div class="col-xs-12 relative">
                            <img class="pitch-grisley-g" alt="Grisley" src="/img/grisley_g.png">
                            <div class="pitch-tagline-wrapper center-absolute">
                                <h1 class="pitch-tagline">Loaders & Transfers</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="products-section">

                <section id="box-plenum-header-section" class="col-xs-12 product-section">
                    <div class="row row-top-buffer-small">
                        <!--<div class="col-lg-offset-1">
                            <ol class="breadcrumb">
                                <li><a href="/products/products.php">All Products</a></li>
                                <li class="active">Loaders</li>
                            </ol>
                        </div> -->
                    </div>
                    <div class="row row-top-buffer-small flexbox-center-vertically flexbox-center-vertically-disable-xs">
                        <div class="col-lg-offset-1 col-lg-5 col-sm-6">
                            <img alt="Loader" src="/img/Loader.jpg" class="img-responsive product-image">
                        </div>
                        <div class="col-lg-5 col-sm-6">
                            <h2 class="product-name">Grisley Loaders</h2>
                            <p><strong>Grisley Loaders</strong> – Proper conveyor loading is vital to the success and final performance of all projects. Grisley Loaders are specifically designed for the unique loading conditions of air-supported conveyors. Our loaders provide superior maintenance access while allowing a dust-tight design and space for adequate flow volumes. Our Internal chutes allow for geometric customization to optimize material flow and conveying performance.</p>
                            <img alt="Loader Diagram" src="/img/Transparent-Loader.jpg" class="img-responsive product-additional-graphic">
                        </div>
                    </div>
                    <div class="row row-top-buffer">
                        <hr class="col-lg-offset-1 col-lg-10">
                    </div>
                    <div class="row row-top-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                        <div class="col-lg-offset-1 col-lg-5 col-sm-6 col-lg-push-5 col-sm-push-6">
                            <img alt="Loaders 4" src="/img/Loaders-4.jpg" class="product-image img-responsive">
                        </div>
                        <div class="col-lg-5 col-sm-6 col-lg-pull-5 col-sm-pull-6">
                            <h2 class="product-name">Grisley Loaders</h2>
                            <p class="product-description"><strong>Grisley Loaders</strong> – Proper conveyor loading is vital to the success and final performance of all projects. Grisley ASC utilizes DEM analysis technology to eliminate the problems associated with bulk material flow the first time.</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <img alt="Loaders 3" src="/img/Loaders-3.jpg" class="img-responsive product-additional-graphic">
                        </div>
                    </div>
                    <div class="row row-top-buffer">
                        <hr class="col-lg-offset-1 col-lg-10">
                    </div>
                    <div class="row row-top-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                        <div class="col-lg-offset-1 col-lg-5 col-sm-6">
                            <img alt="Head Frame" src="/img/Head-Frame.jpg" class="product-image img-responsive">
                        </div>
                        <div class="col-lg-5 col-sm-6">
                            <h2 class="product-name">Grisley Transfers</h2>
                            <p class="product-description"><strong>Grisley Head Frames</strong> – Proper material transfer is vital to the success and final performance of all projects. Grisley Head Frames are specifically designed for the unique transfer conditions for the material being conveyed. Our Head Frames provide superior cleaning ability with maintenance access while allowing a dust-tight design and space for adequate flow volumes. Our Internal chutes allow for geometric customization to optimize material flow and conveying performance.</p>
                            <p>&nbsp;</p>
                            <img alt="Head Frame Wireframe" src="/img/Head-Frame-Wireframe.jpg" class="img-responsive product-additional-graphic">
                        </div>
                    </div>
                    <div class="row row-top-buffer">
                        <hr class="col-lg-offset-1 col-lg-10">
                    </div>
                    <div class="row row-top-buffer row-bottom-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                        <div class="col-lg-offset-1 col-lg-5 col-sm-6 col-lg-push-5 col-sm-push-6">
                            <img alt="Head Transition & Scavenger Exploded" src="/img/Head-Transition-&-Scavenger-Exploded.jpg" class="product-image img-responsive">
                        </div>
                        <div class="col-lg-5 col-sm-6 col-lg-pull-5 col-sm-pull-6">
                            <h2 class="product-name">Grisley Transition Cleaning</h2>
                            <p class="product-description"><strong>Grisley Head Frames</strong> – Proper material transitions and belt cleaning is vital to the success and final performance of all projects. Our Head Frames provide superior cleaning ability with maintenance access while allowing a dust-tight design and space for adequate flow volumes. Grisley also has the ability to provide custom solutions for your specific belt cleaning needs.</p>
                            <img alt="Head Transition & Scavenger" src="/img/Head-Transition-&-Scavenger.jpg" class="img-responsive product-additional-graphic">
                        </div>
                    </div>
                </section>
                
            </section>

        </div>
        
        <?php
            $footerTemplate = new Template("$root/php/html_templates/footer.php");
            echo $footerTemplate;
        ?>
    </body>
</html>
