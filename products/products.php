<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            include("$root/php/html_templates/template.php");
            
            $headContentTemplate = new Template("$root/php/html_templates/head_content.php");
            $headContentTemplate->title = "Products";
            echo $headContentTemplate;
        ?>
    </head>
    <body id="body-products">
		<?php include_once("analyticstracking.php") ?>
        
        <?php
            $loadingScreenTemplate = new Template("$root/php/html_templates/loading_screen.php");
            echo $loadingScreenTemplate;
        ?>
        
        <?php
            $headerTemplate = new Template("$root/php/html_templates/header.php");
            $headerTemplate->allProductsActive = true;
            echo $headerTemplate;
        ?>
        
        <div id="content" class="container-fluid">
            
            <?php
                $brochureRibbonTemplate = new Template("$root/php/html_templates/brochure_ribbon.php");
                echo $brochureRibbonTemplate;
            ?>
            
            <section id="products-header-wrapper" class="pitch-wrapper flexbox-center-vertically">
                <div id="products-header" class="pitch">
                    <div class="row row-top-buffer-small row-bottom-buffer-small">
                        <div class="col-xs-12 relative">
                            <img class="pitch-grisley-g" alt="Grisley" src="/img/grisley_g.png">
                            <div class="pitch-tagline-wrapper center-absolute">
                                <h1 class="pitch-tagline">Our Products</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        
            <section id="products-section">
                <div class="row row-top-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6">
                        <img alt="Box Plenum" src="/img/Box-Plenum-New.jpg" class="product-image img-responsive">
                    </div>
                    <div class="col-lg-5 col-sm-6">
                        <h2 class="product-name">Box Plenum&trade;</h2>
                        <p class="product-description"><strong>The Grisley ASC Box Plenum&trade;</strong> – Is specifically designed for new system installations. Our design provides a dust-tight and weather proof air supported system on both the carry and return sides of the belt that eliminates the use of traditional conveyor rollers and eliminates the maintenance associated with conventional roller-based conveyors. This system also eliminates the need for process air and baghouse systems. See our brochure for more in-depth performance capabilities of our Box Plenum&trade;.</p>
                        <p class="learn-more"><a href="/products/box_plenum.php">Click to learn more <span class="glyphicon glyphicon-chevron-right"></span></a></p>
                    </div>
                </div>
                <div class="row row-top-buffer">
                    <hr class="col-lg-offset-1 col-lg-10">
                </div>
                <div class="row row-top-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6 col-lg-push-5 col-sm-push-6">
                        <img alt="Retrofit Plenum" src="/img/Retrofit-Plenum.jpg" class="product-image img-responsive">
                    </div>
                    <div class="col-lg-5 col-sm-6 col-lg-pull-5 col-sm-pull-6">
                        <h2 class="product-name">Retrofit Plenum</h2>
                        <p class="product-description"><strong>Grisley Retrofit</strong> – Grisley's air-supported retrofit plenum allows our customers to retrofit an existing traditional roller-based conveyor with a dust-tight and weather proof air supported system on the carrying side. The retrofit plenum is frequently used to help eliminate dust emitting systems while also eliminating the need for process air and baghouse systems. See our brochure for more in-depth capabilites of our Retrofit Plenum.</p>
                        <p class="learn-more"><a href="/products/retrofit_plenum.php">Click to learn more <span class="glyphicon glyphicon-chevron-right"></span></a></p>
                    </div>
                </div>
                <div class="row row-top-buffer">
                    <hr class="col-lg-offset-1 col-lg-10">
                </div>
                <div class="row row-top-buffer row-bottom-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6">
                        <img alt="Loader" src="/img/Loader.jpg" class="product-image img-responsive">
                    </div>
                    <div class="col-lg-5 col-sm-6">
                        <h2 class="product-name">Grisley Loaders</h2>
                        <p class="product-description"><strong>Grisley Loaders</strong> – Proper conveyor loading is vital to the success and final performance of all projects. Grisley Loaders are specifically designed for the unique loading conditions of air-supported conveyors. Our loaders provide superior maintenance access while allowing a dust-tight design and space for adequate flow volumes. Our Internal chutes allow for geometric customization to optimize material flow and conveying performance.</p>
                        <p class="learn-more"><a href="/products/loaders.php">Click to learn more <span class="glyphicon glyphicon-chevron-right"></span></a></p>
                    </div>
                </div>
            </section>
                
        </div>
        
        <?php
            $footerTemplate = new Template("$root/php/html_templates/footer.php");
            echo $footerTemplate;
        ?>
    </body>
</html>
