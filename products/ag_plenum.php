<!DOCTYPE html>
<html>
    <head>
        <?php
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            include("$root/php/html_templates/template.php");
            
            $headContentTemplate = new Template("$root/php/html_templates/head_content.php");
            $headContentTemplate->title = "Products - AG Plenum";
            echo $headContentTemplate;
        ?>
    </head>
    <body id="body-products">
		<?php include_once("analyticstracking.php") ?>
        
        <?php
            $loadingScreenTemplate = new Template("$root/php/html_templates/loading_screen.php");
            echo $loadingScreenTemplate;
        ?>
        
        <?php
            $headerTemplate = new Template("$root/php/html_templates/header.php");
            $headerTemplate->agPlenumActive = true;
            echo $headerTemplate;
        ?>
        
        <div id="content" class="container-fluid">
            
            <?php
                $brochureRibbonTemplate = new Template("$root/php/html_templates/brochure_ribbon.php");
                echo $brochureRibbonTemplate;
            ?>

            <section id="products-header-wrapper" class="pitch-wrapper flexbox-center-vertically">
                <div id="products-header" class="pitch">
                    <div class="row row-top-buffer-small row-bottom-buffer-small">
                        <div class="col-xs-12 relative">
                            <img class="pitch-grisley-g" alt="Grisley" src="/img/grisley_g.png">
                            <div class="pitch-tagline-wrapper center-absolute">
                                <h1 class="pitch-tagline">Air Glide Plenum</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="products-section">

                <section id="box-plenum-header-section" class="col-xs-12 product-section">
                    <div class="row row-top-buffer-small">
                        <!--<div class="col-lg-offset-1">
                            <ol class="breadcrumb">
                                <li><a href="/products/products.php">All Products</a></li>
                                <li class="active">Loaders</li>
                            </ol>
                        </div> -->
                    </div>
                    <div class="row row-top-buffer-small flexbox-center-vertically flexbox-center-vertically-disable-xs">
                        <div class="col-lg-offset-1 col-lg-5 col-sm-6">
                            <img alt="Air Glide Fitup Assembly" src="/img/Air-Glide-1.jpg" class="img-responsive product-image">
                        </div>
                        <div class="col-lg-5 col-sm-6">
                            <h2 class="product-name">Grisley Air Glide</h2>
                            <p><strong>The Grisley Air Glide</strong> is an air supported conveyor designed specifically for the agriculture industry. It takes the place of regular spool type conveyors. Like our other models, this is a completely enclosed air supprted conveyor with an air supported return and its own dust collection system. The advantages are in its simple design, easy construction, and being priced very competitive to spool type conveyors.</p>
                        </div>
                    </div>
                    <div class="row row-top-buffer">
                        <hr class="col-lg-offset-1 col-lg-10">
                    </div>
                    <div class="row row-top-buffer row-bottom-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                        <div class="col-lg-offset-1 col-lg-5 col-sm-6 col-lg-push-5 col-sm-push-6">
                            <img alt="Air Glide Conveyor vs Hi Roller" src="/img/Air-Glide-2.jpg" class="product-image img-responsive">
                        </div>
                        <div class="col-lg-5 col-sm-6 col-lg-pull-5 col-sm-pull-6">
                            <h2 class="product-name">Grisley Air Glide</h2>
                            <p class="product-description"><strong>The Grisley Air Glide</strong> uses a unique design to remove the use of spool idlers. No spools or bearings means less maintenance and less downtime. There are no moving parts throughout the length the conveyor, except at the head and tail sections. The Grisley Air Glide offers a doggedly reliable conveyor.</p>
                        </div>
                    </div>
                </section>
                
            </section>

        </div>
        
        <?php
            $footerTemplate = new Template("$root/php/html_templates/footer.php");
            echo $footerTemplate;
        ?>
    </body>
</html>
