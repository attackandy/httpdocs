<!DOCTYPE html>
<html>
    <head>
        <?php
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            include("$root/php/html_templates/template.php");
            
            $headContentTemplate = new Template("$root/php/html_templates/head_content.php");
            $headContentTemplate->title = "Products - Retrofit Plenum";
            echo $headContentTemplate;
        ?>
    </head>
    <body id="body-products">
		<?php include_once("analyticstracking.php") ?>
        
        <?php
            $loadingScreenTemplate = new Template("$root/php/html_templates/loading_screen.php");
            echo $loadingScreenTemplate;
        ?>
        
        <?php
            $headerTemplate = new Template("$root/php/html_templates/header.php");
            $headerTemplate->retrofitPlenumActive = true;
            echo $headerTemplate;
        ?>
        
        <div id="content" class="container-fluid">
            
            <?php
                $brochureRibbonTemplate = new Template("$root/php/html_templates/brochure_ribbon.php");
                echo $brochureRibbonTemplate;
            ?>

            <section id="products-header-wrapper" class="pitch-wrapper flexbox-center-vertically">
                <div id="products-header" class="pitch">
                    <div class="row row-top-buffer-small row-bottom-buffer-small">
                        <div class="col-xs-12 relative">
                            <img class="pitch-grisley-g" alt="Grisley" src="/img/grisley_g.png">
                            <div class="pitch-tagline-wrapper center-absolute">
                                <h1 class="pitch-tagline">Retrofit Plenum</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="products-section">
            
                <section id="v-plenum-header-section" class="col-xs-12 product-section">
                    <div class="row row-top-buffer-small">
                        <!-- <div class="col-lg-offset-1">
                            <ol class="breadcrumb">
                                <li><a href="/products/products.php">All Products</a></li>
                                <li class="active">Retrofit Plenum</li>
                            </ol>
                        </div> -->
                    </div>
                    <div class="row row-top-buffer-small flexbox-center-vertically flexbox-center-vertically-disable-xs">
                        <div class="col-lg-offset-1 col-lg-5 col-sm-6">
                            <img alt="V Plenum" src="/img/Retrofit-Plenum.jpg" class="img-responsive product-image">
                        </div>
                        <div class="col-lg-5 col-sm-6">
                            <h2 class="product-name">Retrofit Plenum</h2>
                            <p><strong>Grisley Retrofit</strong> – Grisley's air-supported retrofit plenum allows our customers to retrofit an existing traditional roller-based conveyor with a dust-tight and weather proof air supported system on the carrying side. The retrofit plenum is frequently used to help eliminate dust emitting systems while also eliminating the need for process air and baghouse systems. See our brochure for more in-depth capabilites of our Retrofit Plenum.</p>
                        </div>
                    </div>

                    <div class="row row-top-buffer">
                        <hr class="col-lg-offset-1 col-lg-10">
                    </div>
                </section>
                
                <section id="v-plenum-upgrade" class="col-xs-12 product-section">
                    <div class="row row-top-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                        <div class="col-lg-offset-1 col-lg-5 col-sm-6 col-lg-push-5 col-sm-push-6">
                            <img alt="Retrofit Plenum Diagram" src="/img/Retrofit-Plenum-Zoom.jpg" class="img-responsive product-additional-graphic" style="max-height:600px">
                        </div>
                        <div class="col-lg-5 col-sm-6 col-lg-pull-5 col-sm-pull-6">
                            <h3><strong>Upgrade</strong> your existing conveyor</h3>
                            <p>Grisley ASC is revolutionizing the way conveying systems control dust. With the combination of Grisley ASC's dust-tight conveyor design and Donaldsons CPV filtering technology we are cutting the costs and inefficiencies associated with dust collection. Design according to CEMA (Conveyor Equipment Manufactures Association) standards. The Retrofit Plenum utilizes the existing drive mechanisms and belt.</p>
                        </div>
                    </div>
                    
                    <div class="row row-top-buffer">
                        <hr class="col-lg-offset-1 col-lg-10">
                    </div>
                </section>
                
                <section id="v-plenum-simplify" class="col-xs-12 product-section">
                    <div class="row row-top-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                        <div class="col-lg-offset-1 col-lg-5 col-sm-6">
                            <img alt="Retrofit Plenum Diagram" src="/img/Retrofit-on-stringers.jpg" class="img-responsive product-additional-graphic" style="max-height:400px">
                        </div>
                        <div class="col-lg-5 col-sm-6">
                            <h3><strong>Simplify</strong> the retrofitting process and <strong>reduce</strong> cost</h3>
                            <p>The Retrofit Plenum ships to site as a modular design that easily takes the place of the conventional conveyor troughing idlers. This requires very little to no upgrades of the existing conveyor support structure. This has an added advantage of eliminating the need for further retrofitting. by eliminating the source of friction and wear on moving parts. the retrofit plenum will <strong>substantially reduce the conveyor’s operating and maintenance costs</strong>.</p>
                            <img alt="Retrofit Plenum Diagram" src="/img/Retrofit-Cross-Section-With-Roller.jpg" class="img-responsive product-additional-graphic">
                        </div>
                    </div>
                    
                    <div class="row row-top-buffer">
                        <hr class="col-lg-offset-1 col-lg-10">
                    </div>
                </section>
                
                <section id="v-plenum-benefits" class="col-xs-12 product-section">
                    <div class="row row-top-buffer row-bottom-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                        <div class="col-lg-offset-1 col-lg-5 col-sm-6 col-lg-push-5 col-sm-push-6 relative">
                            <i class="fa fa-circle-thin center-absolute play-video"></i>
                            <div class="center-absolute play-video-text">
                                <p style="padding:0;margin:0;text-align:center">Watch<br>Video</p>
                            </div>
                            <video class="video-responsive product-additional-graphic" style="max-width:352px">
                                <source src="/vid/DM-3-1.mp4" type="video/mp4">
                            </video>
                        </div>
                        <div class="col-lg-5 col-sm-6 col-lg-pull-5 col-sm-pull-6">
                            <h3><strong>All</strong> of the benefits of air-supported conveying</h3>
                            <p>The Retrofit Plenum is available at a competitive per-section cost compared to conventional rollers. The retrofit design provides all of the inherent benefits of air-supported conveying on the carrying side with <strong>substantial reduction in installation costs and downtime</strong>.</p>
                            <p class="contact-us"><a href="/contact.php">Contact us today for a quote <span class="glyphicon glyphicon-chevron-right"></span></a></p>
                        </div>
                    </div>
                </section>

            </section>

        </div>
        
        <?php
            $footerTemplate = new Template("$root/php/html_templates/footer.php");
            echo $footerTemplate;
        ?>
    </body>
</html>
