<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            include("$root/php/html_templates/template.php");
            
            $headContentTemplate = new Template("$root/php/html_templates/head_content.php");
            $headContentTemplate->title = "Products";
            echo $headContentTemplate;
        ?>
    </head>
    <body id="body-products">
        
        <?php
            $loadingScreenTemplate = new Template("$root/php/html_templates/loading_screen.php");
            echo $loadingScreenTemplate;
        ?>
        
        <?php
            $headerTemplate = new Template("$root/php/html_templates/header.php");
            $headerTemplate->productsActive = true;
            echo $headerTemplate;
        ?>
        
        <div id="content" class="container-fluid">
            
            <section id="products-header-wrapper" class="pitch-wrapper flexbox-center-vertically">
                <div id="products-header" class="pitch">
                    <div class="row row-top-buffer-small row-bottom-buffer-small">
                        <div class="col-xs-12 relative">
                            <img class="pitch-grisley-g" alt="Grisley" src="/img/grisley_g.png">
                            <div class="pitch-tagline-wrapper center-absolute">
                                <h1 class="pitch-tagline">Our Products</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        
            <section id="products-section">
                <div class="row row-top-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6">
                        <img alt="Box Plenum" src="/img/box_plenum.png" class="product-image img-responsive">
                    </div>
                    <div class="col-lg-5 col-sm-6">
                        <h2 class="product-name">Box Plenum&trade;</h2>
                        <p class="product-description"><strong>The Grisley ASC Box Plenum&trade;</strong> is specifically designed for new installations, where 
                            fugitive materials such as dust must be controlled and eliminated.  The Box Plenum&trade; design  
                            allows for an air-supported return, which eliminates  the problems caused by dust on the return 
                            side of the conveyor.  The Box Plenum&trade; is frequently incorporated as a shuttle conveyor, as well 
                            as a ship loader.</p>
                        <p class="learn-more"><a href="/box_plenum.html">Click to learn more <span class="glyphicon glyphicon-chevron-right"></span></a></p>
                    </div>
                </div>
                <div class="row row-top-buffer">
                    <hr class="col-lg-offset-1 col-lg-10">
                </div>
                <div class="row row-top-buffer row-bottom-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6 col-lg-push-5 col-sm-push-6">
                        <img alt="V Plenum" src="/img/v_plenum.png" class="product-image img-responsive">
                    </div>
                    <div class="col-lg-5 col-sm-6 col-lg-pull-5 col-sm-pull-6">
                        <h2 class="product-name">V Plenum&trade;</h2>
                        <p class="product-description"><strong>The V Plenum&trade;</strong> is Grisley ASC’s patented retrofit design.  The V Plenum&trade; allows the user to 
                            retrofit an existing roller-based conveyor with an air-supported carrying side that is dust tight and 
                            weatherproof.  The V Plenum&trade; is frequently used in power generation facilities in order to control dusting 
                            issues caused by coal handling.  Because it is dust-tight, the V Plenum&trade; is an ideal load zone 
                            or transfer point conveyor that will eliminate troublesome and dangerous situations.</p>
                        <p class="learn-more"><a href="/v_plenum.html">Click to learn more <span class="glyphicon glyphicon-chevron-right"></span></a></p>
                    </div>
                </div>
            </section>
                
        </div>
        
        <?php
            $footerTemplate = new Template("$root/php/html_templates/footer.php");
            echo $footerTemplate;
        ?>
    </body>
</html>
