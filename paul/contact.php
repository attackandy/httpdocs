<!DOCTYPE html>
<html>
    <head>
        <?php
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            include("$root/php/html_templates/template.php");
            
            $headContentTemplate = new Template("$root/php/html_templates/head_content.php");
            $headContentTemplate->title = "Contact Us";
            echo $headContentTemplate;
        ?>
    </head>
    <body id="body-contact" class="body-background">
        
        <?php
            $loadingScreenTemplate = new Template("$root/php/html_templates/loading_screen.php");
            echo $loadingScreenTemplate;
        ?>
        
        <?php
            $headerTemplate = new Template("$root/php/html_templates/header.php");
            $headerTemplate->contactActive = true;
            echo $headerTemplate;
        ?>
        
        <div id="content" class="container-fluid">
            
            <section id="contact-section">
                
                <div class="row row-top-buffer-small row-bottom-buffer">
                    <div id="additional-contact-info" class="col-xs-12 col-md-offset-1 col-md-3 col-md-push-7">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-12 additional-contact-info-item">
                                <h4 class="flexbox-center-vertically"><i class="fa fa-map-marker additional-contact-info-item-icon"></i>Address</h4>
                                <a id="footer-contact-link-map" href="https://www.google.com/maps/place/10+West+Broadway+Suite+500+84101" target="_blank">
                                    <span>10 W. Broadway #500<br>Salt Lake City, UT 84101</span>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-12 additional-contact-info-item">
                                <h4 class="flexbox-center-vertically"><i class="fa fa-envelope additional-contact-info-item-icon"></i>E-mail</h4>
                                <a id="footer-contact-link-mail" href="mailto:info@grisley.com">info@grisley.com</a>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-12 additional-contact-info-item">
                                <h4 class="flexbox-center-vertically"><i class="fa fa-phone-square additional-contact-info-item-icon"></i>Phone</h4>
                                <a href="tel:13037566474">+1 303-756-6474</a>
                            </div>
                            <div class="col-xs-12 additional-contact-info-item flexbox-center-vertically">
                                <span class="additional-contact-info-item-social-media-header">Follow us:</span>
                                <a id="facebook-link" class="additional-contact-info-item-social-media-icon icon-facebook" href="https://www.facebook.com/grisley.air.supported.conveyors" target="_blank">
                                    <i class="fa fa-facebook center-absolute"></i>
                                </a>
                                <a id="youtube-link" class="additional-contact-info-item-social-media-icon icon-youtube" href="https://www.youtube.com/channel/UC0wGRXb5Bf3UiHTAg5CHbLA" target="_blank">
                                    <i class="fa fa-youtube center-absolute"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="contact-form-wrapper" class="col-xs-12 col-md-7 col-md-pull-3">
                        <?php
                            $contactFormTemplate = new Template("$root/php/html_templates/contact_form.php");
                            $contactFormTemplate->contactFormSubHeader = "Request a quote, leave a comment, or ask a question";
                            echo $contactFormTemplate;
                        ?>
                    </div>
                </div>
                
            </section>

        </div>
        
        <?php
            $footerTemplate = new Template("$root/php/html_templates/footer.php");
            echo $footerTemplate;
        ?>
    </body>
</html>

