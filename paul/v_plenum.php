<!DOCTYPE html>
<html>
    <head>
        <?php
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            include("$root/php/html_templates/template.php");
            
            $headContentTemplate = new Template("$root/php/html_templates/head_content.php");
            $headContentTemplate->title = "Products - V Plenum";
            echo $headContentTemplate;
        ?>
    </head>
    <body>
        
        <?php
            $loadingScreenTemplate = new Template("$root/php/html_templates/loading_screen.php");
            echo $loadingScreenTemplate;
        ?>
        
        <?php
            $headerTemplate = new Template("$root/php/html_templates/header.php");
            $headerTemplate->vPlenumActive = true;
            echo $headerTemplate;
        ?>
        
        <div id="content" class="container-fluid">
            
            <section id="v-plenum-header-section" class="col-xs-12 product-section">
                <div class="row row-top-buffer-small">
                    <div class="col-lg-offset-1">
                        <ol class="breadcrumb">
                            <li><a href="/products.html">All Products</a></li>
                            <li class="active">V Plenum&trade;</li>
                        </ol>
                    </div>
                </div>
                <div class="row row-top-buffer-small flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6">
                        <img alt="V Plenum" src="/img/v_plenum.png" class="img-responsive product-image">
                    </div>
                    <div class="col-lg-5 col-sm-6">
                        <h2 class="product-name">V Plenum&trade;</h2>
                        <p><strong>The V Plenum&trade;</strong> is Grisley ASC’s patented retrofit design.  The V Plenum&trade; allows the user to 
                            retrofit an existing roller-based conveyor with an air-supported carrying side that is dust tight and 
                            weatherproof.  The V Plenum&trade; is frequently used in power generation facilities in order to control dusting 
                            issues caused by coal handling.  Because it is dust-tight, the V Plenum&trade; is an ideal load zone 
                            or transfer point conveyor that will eliminate troublesome and dangerous situations.</p>
                    </div>
                </div>

                <div class="row row-top-buffer">
                    <hr class="col-lg-offset-1 col-lg-10">
                </div>
            </section>
            
            <section id="v-plenum-upgrade" class="col-xs-12 product-section">
                <div class="row row-top-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6 col-lg-push-5 col-sm-push-6">
                        <img alt="V Plenum Diagram" src="/img/v_plenum_diagram.jpg" class="img-responsive product-additional-graphic" style="max-height:600px">
                    </div>
                    <div class="col-lg-5 col-sm-6 col-lg-pull-5 col-sm-pull-6">
                        <h3><strong>Upgrade</strong> your existing conveyor</h3>
                        <p>The patented Grisley V Plenum&trade; Air Supported Conveyor is a component-based method for upgrading 
                            troughing conveyors to <strong>more reliable and cost-effective</strong> air-supported conveyors. 
                            Designed according to CEMA (Conveyor Equipment Manufacturers Association) standards, the V Plenum&trade; 
                            utilizes the troughing conveyor’s existing support structure, drive mechanism and belt.</p>
                    </div>
                </div>
                
                <div class="row row-top-buffer">
                    <hr class="col-lg-offset-1 col-lg-10">
                </div>
            </section>
            
            <section id="v-plenum-simplify" class="col-xs-12 product-section">
                <div class="row row-top-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6">
                        <img alt="V Plenum Diagram" src="/img/v_plenum_diagram_2.jpg" class="img-responsive product-additional-graphic" style="max-height:400px">
                    </div>
                    <div class="col-lg-5 col-sm-6">
                        <h3><strong>Simplify</strong> the retrofitting process and <strong>reduce</strong> the cost</h3>
                        <p>Using the technology that is quickly making Grisley ASC the industry standard for high performance 
                            conveying, the V Plenum&trade; is more efficient, reliable and cost-effective than conventional conveying 
                            technologies. Our retrofitting method not only simplifies the process of upgrading troughing conveyors, 
                            but it has the added advantage of eliminating the need for further retrofitting. By eliminating the 
                            source of wear (friction from moving parts), the V Plenum&trade; will <strong>substantially reduce the conveyor’s 
                            operating and maintenance costs</strong>.</p>
                    </div>
                </div>
                
                <div class="row row-top-buffer">
                    <hr class="col-lg-offset-1 col-lg-10">
                </div>
            </section>
            
            <section id="v-plenum-benefits" class="col-xs-12 product-section">
                <div class="row row-top-buffer row-bottom-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6 col-lg-push-5 col-sm-push-6 relative">
                        <i class="fa fa-circle-thin center-absolute play-video"></i>
                        <div class="center-absolute play-video-text">
                            <p style="padding:0;margin:0;text-align:center">Watch<br>Video</p>
                        </div>
                        <video class="video-responsive product-additional-graphic" style="max-width:352px">
                            <source src="/vid/DM-3-1.mp4" type="video/mp4">
                        </video>
                    </div>
                    <div class="col-lg-5 col-sm-6 col-lg-pull-5 col-sm-pull-6">
                        <h3><strong>All</strong> of the benefits of air-supported conveying</h3>
                        <p>The V Plenum&trade; ASC is available at a per-section cost comparable to that of a conventional and regularly-scheduled retrofit. 
                            The V Plenum&trade; retrofit provides all of the inherent benefits of air-supported conveying, and because it uses the troughing 
                            conveyor’s existing structure, there is a <strong>substantial reduction in installation costs and downtime</strong>.</p>
                        <p class="contact-us"><a href="/contact.html">Contact us today for a quote <span class="glyphicon glyphicon-chevron-right"></span></a></p>
                    </div>
                </div>
            </section>

        </div>
        
        <?php
            $footerTemplate = new Template("$root/php/html_templates/footer.php");
            echo $footerTemplate;
        ?>
    </body>
</html>
