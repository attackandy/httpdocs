var GRISLEY_GLOBAL_VARS = {
};

$(document).ready(function() {

    var $loadingScreen = $('#loading-screen');
    var $loadingIcon = $('#loading-icon');
    $loadingIcon.hide();
    var loadingTimeout = window.setTimeout(function() {
        $loadingIcon.fadeIn(100);
    }, 1000);
    
    $('.dot-nav-tooltip').tooltip({
        placement: 'left'
    });   

    $(window).bind('scroll',function(e){
        dotnavigation();
    });
    
    function dotnavigation(){
             
        var numSections = $('section').length;
        
        $('#dot-nav li a').removeClass('active').parent('li').removeClass('active');     
        $('section').each(function(i,item){
            var ele = $(item), nextTop;

            if (typeof ele.next().offset() !== "undefined") {
                nextTop = ele.next().offset().top;
            }
            else {
                nextTop = $(document).height();
            }

            if (ele.offset() !== null) {
                thisTop = ele.offset().top - ((nextTop - ele.offset().top) / numSections);
            }
            else {
                thisTop = 0;
            }

            var docTop = $(document).scrollTop();

            if (docTop >= thisTop && (docTop < nextTop)){
                $('#dot-nav li').eq(i).addClass('active');
            }
        });   
    }

    $('#dot-nav li').click(function(){
      
        var id = $(this).find('a').attr("href"),
            posi,
            ele,
            padding = 100;
      
        ele = $(id);
        posi = ($(ele).offset()||0).top - padding;
      
        $('html, body').animate({
            scrollTop: posi
        }, 'slow');
      
        return false;
    });
    
    var updateCopyrightDate = function() {
        var date = new Date();
        $('#copyright-year').text(date.getFullYear());
    };

    updateCopyrightDate();
    
$(window).load(function() {
    
    clearTimeout(loadingTimeout);
    
    var padBody = function() {  
        $('body').css('padding-bottom', $('footer').outerHeight());
    };
    var fullscreenifyPitch = function() {
        var $elemHomePitchWrapper = $('#home-pitch-wrapper');
        var $elemHomePitch = $('#home-pitch');

        if ($elemHomePitch.outerHeight() < window.innerHeight - 80) {
            $elemHomePitchWrapper.css('min-height', window.innerHeight - 80);
            $elemHomePitchWrapper.css('margin-bottom', 0);
        }
        else {
            $elemHomePitchWrapper.css('min-height', 0);
            $elemHomePitchWrapper.css('margin-bottom', 100);
        }
        
        //IE fix - flexbox vertical centering needs a height, so set the height
        //to the height of the content
        $elemHomePitchWrapper.css('height', $elemHomePitch.outerHeight());
    };
    var didResize = false;
    
    function fadeIn($elem, actionBeforeFade, actionAfterFade) {
        if (typeof(actionBeforeFade) !== 'undefined') {
            actionBeforeFade();
        }
        
        $elem.fadeTo(1000, 1.0, function () {
            if ($elem.hasClass("not-visible")) {
                $elem.removeClass("not-visible");
            }
            
            if (typeof(actionAfterFade) !== 'undefined') {
                actionAfterFade();
            }
        });
        
        $elem.children().each(function() {
           var $this = $(this);
           $this.fadeTo(1000, 1.0, function () {
                if ($this.hasClass("not-visible")) {
                    $this.removeClass("not-visible");
                }
            });
        });
    }
    
    function handleScrolledTo(elemId, actionBeforeFade, actionAfterFade) {
        var $elem = $('#' + elemId);
        if ($elem.length !== 0) {
            new Waypoint({ 
                element: $elem,
                offset: "50%",
                handler: function(direction) {
                    if (!$elem.hasClass("no-more-actions")) {
                        $elem.addClass("no-more-actions");
                        fadeIn($elem, actionBeforeFade, actionAfterFade);
                    }
                }
            });
        }
    }
    
    $(window).resize(function() {
        didResize = true;
    });
    
    if ($loadingIcon.css('display') !== 'none') {
        $loadingIcon.fadeOut(500, function() {
            $loadingIcon.hide();
            $loadingScreen.fadeOut(1000, function() {
                $loadingScreen.hide();
            });
        });
    }
    else {
        $loadingScreen.fadeOut(1000, function() {
            $loadingScreen.hide();
        });
    }
    
    fullscreenifyPitch();
    padBody();
    
    setInterval(function() {  
        if(didResize) {
            didResize = false;
            fullscreenifyPitch();
            padBody();
            $('.header-underline').each(function () {
                var $this = $(this);
                var $headerSpan = $this.siblings('h1').find('span');
                if ($this.width() > $headerSpan.width() ||
                    ($this.width() < $headerSpan.width() && $this.width() > 0)) {
                    $this.width($this.siblings('h1').find('span').width());
                }
            });
            
            Waypoint.refreshAll();
        }
    }, 250);
    
    $('#home-pitch-button-why').on('click', function () {
        $('html, body').animate({
            scrollTop: $('#home-advantages-section').offset().top - 100
        }, 'slow');
        return false;
    });
    
    handleScrolledTo("home-advantages-section", function () {
        $('#header-advantages-underline').animate({
            width: $('#header-advantages > h1 span').width()
        }, 1000);
    }, function() {
        fadeIn($("#advantage-icon-click-me-arrow"));
    });
    
    if (Modernizr.touch) { 
        $('.advantage-icon-wrapper').bind('touchstart', function() {
            var $this = $(this);
            $this.addClass('hover');
        });
        $('.advantage-icon-wrapper').bind('touchend', function() {
            var $this = $(this);
            $this.removeClass('hover');
        });
    }
    else {
        $('.advantage-icon-wrapper').hover(function() {
            var $this = $(this);
            $this.addClass('hover');
        }, function() {
            var $this = $(this);
            $this.removeClass('hover');
        });
    }
    
    var handleAdvantageIconClick = function(elemIconId, elemTextId) {
        $('#' + elemIconId).on('click', function () {
            var $this = $(this);
            $this.addClass('selected');
            $('#' + $this.attr('data-modal')).modal({show: true});
        });
    };

    handleAdvantageIconClick('advantage-icon-wrapper-environment');
    handleAdvantageIconClick('advantage-icon-wrapper-cost');
    handleAdvantageIconClick('advantage-icon-wrapper-power');
    handleAdvantageIconClick('advantage-icon-wrapper-maintenance');
    handleAdvantageIconClick('advantage-icon-wrapper-volume');
    handleAdvantageIconClick('advantage-icon-wrapper-performance');
    
    $('.modal-advantage').on('hidden.bs.modal', function () {
        $('.advantage-icon-wrapper').each(function () {
            var $this = $(this);
            if ($this.hasClass('selected')) {
                $this.removeClass('selected');
            }
        });
    });
    
    $('.play-video').on('click', function () {
        var $video = $(this).siblings('video');
        var $playVideoText = $(this).siblings('.play-video-text');
        $(this).css('display', 'none');
        $playVideoText.css('display', 'none');
        $video.css('opacity', 1);
        $video.get(0).play();
    });
    
    $('video').on('ended', function (){
        var $playVideo = $(this).siblings('.play-video');
        var $playVideoText = $(this).siblings('.play-video-text');
        $(this).css('opacity', 0.25);
        $playVideo.css('display', 'block');
        $playVideoText.css('display', 'block');
    });
    
    $('#modal-close').on('click', function () {
        $('.advantage-icon-circle').each(function () {
            var $thisIcon = $(this);
            if ($thisIcon.hasClass('selected')) {
                $thisIcon.removeClass('selected');
                $thisIcon.siblings('.advantage-icon').removeClass('selected');
            }
        });
    });
    
    handleScrolledTo("home-projects-section", function () {
        $('#header-projects-underline').animate({
            width: $('#header-projects > h1 span').width()
        }, 1000);
    });
    
    handleScrolledTo("home-contact-section", function () {
        $('#header-contact-underline').animate({
            width: $('#header-contact > h1 span').width()
        }, 1000);
    });
    
    var validateContactForm = function() {
        var $contactForm = $('#contact-form');
        if ($contactForm.length > 0) {

            $contactForm.validator({
                disable: false
            });
            
            var $feedback_spinner = $('.feedback-message-wrapper .fa-spinner');
            $feedback_spinner.hide();
            
            $contactForm.validator().on('submit', function (e) {
                var $feedback_message = $contactForm.find('.feedback-message');
                
                if (e.isDefaultPrevented()) {
                    $feedback_message.text("Your message was not sent. Please fix the incorrect field(s) and try again.");
                } else {
                    e.preventDefault();
                    
                    $contactForm.find('.form-control').each(function () {
                        $(this).attr('disabled', 'disabled');
                    });
                    
                    $feedback_spinner.show();
                    $feedback_message.text('Sending...');
                    
                    $contactForm.find(':submit').each(function () {
                        $(this).attr('disabled', 'disabled');
                    });
                    
                    var params = {};
                    $contactForm.find('.form-control').each(function () {
                        var $this = $(this);
                        params[$this.attr('name')] = $this.val();
                    });

                    $.post($contactForm.attr('action'), params)
                    .done(function(msg) { 
                        $contactForm[0].reset();
                        $contactForm.find('.form-control').each(function () {
                            var $this = $(this);
                            var $form_group = $this.closest('.form-group');
                            var $feedback = $form_group.find('.form-control-feedback');
                            $form_group.removeClass('has-success');
                            $form_group.removeClass('has-error');
                            $feedback.removeClass('glyphicon-ok');
                            $feedback.removeClass('glyphicon-remove');
                        });
                        $feedback_message.text(msg);
                     })
                    .fail(function(xhr, status, error) {
                        if (xhr.status === 400) {
                            try {
                                var errors_object = JSON.parse(xhr.responseText);
                                for (var key in errors_object) {
                                    if (errors_object[key] === true) {
                                        var $input = $contactForm.find('.form-control[name="' + key + '"]');
                                        var $form_group = $input.closest('.form-group');
                                        var $feedback = $form_group.find('.form-control-feedback');
                                        var $help_block = $form_group.find('.help-block.with-errors');
                                        var error_message = $input.attr('data-error');

                                        $form_group.removeClass('has-success');
                                        $form_group.addClass('has-error');
                                        $feedback.removeClass('glyphicon-ok');
                                        $feedback.addClass('glyphicon-remove');
                                        $help_block.append($('<ul/>').addClass('list-unstyled').append($('<li/>')['text'](error_message)));
                                    }
                                }
                                $feedback_message.text("Your message was not sent. Please fix the incorrect field(s) and try again.");
                            }
                            catch(err) {
                                $feedback_message.text("Internal server error");
                            }
                        }
                        else {
                            $feedback_message.text("Internal server error");
                        }
                    })
                    .always(function() {
                        $feedback_spinner.hide();
                        $contactForm.find('.form-control').each(function () {
                            $(this).removeAttr('disabled');
                        });
                        $contactForm.find(':submit').each(function () {
                            $(this).removeAttr('disabled');
                        });
                    });
                }
            });
        }
    };
    
    validateContactForm();
    
});
});