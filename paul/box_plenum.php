<!DOCTYPE html>
<html>
    <head>
        <?php
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            include("$root/php/html_templates/template.php");
            
            $headContentTemplate = new Template("$root/php/html_templates/head_content.php");
            $headContentTemplate->title = "Products - Box Plenum";
            echo $headContentTemplate;
        ?>
    </head>
    <body>
        
        <?php
            $loadingScreenTemplate = new Template("$root/php/html_templates/loading_screen.php");
            echo $loadingScreenTemplate;
        ?>
        
        <?php
            $headerTemplate = new Template("$root/php/html_templates/header.php");
            $headerTemplate->boxPlenumActive = true;
            echo $headerTemplate;
        ?>
        
        <div id="content" class="container-fluid">

            <section id="box-plenum-header-section" class="col-xs-12 product-section">
                <div class="row row-top-buffer-small">
                    <div class="col-lg-offset-1">
                        <ol class="breadcrumb">
                            <li><a href="/products.html">All Products</a></li>
                            <li class="active">Box Plenum&trade;</li>
                        </ol>
                    </div>
                </div>
                <div class="row row-top-buffer-small flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6">
                        <img alt="Box Plenum" src="/img/box_plenum.png" class="img-responsive product-image">
                    </div>
                    <div class="col-lg-5 col-sm-6">
                        <h2 class="product-name">Box Plenum&trade;</h2>
                        <p><strong>The Grisley ASC Box Plenum&trade;</strong> is specifically designed for new installations, where 
                            fugitive materials such as dust must be controlled and eliminated.  The Box Plenum&trade; design  
                            allows for an air-supported return, which eliminates  the problems caused by dust on the return 
                            side of the conveyor.  The Box Plenum&trade; is frequently incorporated as a shuttle conveyor, as well 
                            as a ship loader.</p>
                    </div>
                </div>

                <div class="row row-top-buffer">
                    <hr class="col-lg-offset-1 col-lg-10">
                </div>
            </section>
            
            <section id="box-plenum-best" class="col-xs-12 product-section">
                <div class="row row-top-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6 col-lg-push-5 col-sm-push-6">
                        <img alt="Award" src="/img/grisley_award.png" class="img-responsive product-additional-graphic">
                    </div>
                    <div class="col-lg-5 col-sm-6 col-lg-pull-5 col-sm-pull-6">
                        <h3>The Industry's <strong>Best</strong> Conveyor</h3>
                        <h4>Setting new standards for high performance</h4>
                        <p>Through its superior engineering our design has made a truly cost-effective air-supported conveyor a 
                            reality. The distinct advantage of the ASC is in its precisely-calibrated plenum design and air-tight 
                            fastening technique. Because of their benefits, the grain industry now looks to air-supported conveyors 
                            as a more efficient alternative to traditional conveying methods. And the Grisley ASC now makes that 
                            alternative <strong>cost-effective and reliable</strong>.</p>
                    </div>
                </div>
                
                <div class="row row-top-buffer">
                    <hr class="col-lg-offset-1 col-lg-10">
                </div>
            </section>
            
            <section id="box-plenum-solves" class="col-xs-12 product-section">
                <div class="row row-top-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6 relative">
                        <i class="fa fa-circle-thin center-absolute play-video"></i>
                        <div class="center-absolute play-video-text">
                            <p style="padding:0;margin:0;text-align:center">Watch<br>Video</p>
                        </div>
                        <video class="video-responsive product-additional-graphic" style="max-width:640px">
                            <source src="/vid/HPIM0859.mp4" type="video/mp4">
                        </video>
                    </div>
                    <div class="col-lg-5 col-sm-6">
                        <h3><strong>Eliminates</strong> mechanical friction</h3>
                        <h4>Renders conventional conveying methods obsolete</h4>
                        <p>Grisley’s engineering solves many of the problems associated with conventional conveyors because it eliminates 
                            their source: mechanical friction. Using air instead of moving parts, its smoother operation makes the Grisley 
                            ASC a better conveyor precisely because it brings the costs of product loss, maintenance, and particle pollution 
                            under control.</p>
                    </div>
                </div>
                
                <div class="row row-top-buffer">
                    <hr class="col-lg-offset-1 col-lg-10">
                </div>
            </section>
            
            <section id="box-plenum-simplicity" class="col-xs-12 product-section">
                <div class="row row-top-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6 col-lg-push-5 col-sm-push-6">
                        <img id="product-additional-graphic-fan" alt="Fan" src="/img/fan.svg" class="img-responsive product-additional-graphic">
                    </div>
                    <div class="col-lg-5 col-sm-6 col-lg-pull-5 col-sm-pull-6">
                        <h3><strong>Simplicity</strong> is key</h3>
                        <p>A low horsepower fan forces air through a calibrated series of holes in a plenum. This thin film of air supports up 
                            to 200 pounds per square foot at high speeds with no mechanical friction. A single fan supports up to 600 feet of 
                            conveyor. This process creates a smooth and even cushion of air on which the conveyor belt glides with ease. Using 
                            this consistent airflow, the Grisley ASC’s belt floats effortlessly in the plenum.</p>
                    </div>
                </div>
                
                <div class="row row-top-buffer">
                    <hr class="col-lg-offset-1 col-lg-10">
                </div>
            </section>
            
            <section id="box-plenum-superior" class="col-xs-12 product-section">
                <div class="row row-top-buffer row-bottom-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <div class="col-lg-offset-1 col-lg-5 col-sm-6">
                        <i class="fa fa-circle-thin center-absolute play-video"></i>
                        <div class="center-absolute play-video-text">
                            <p style="padding:0;margin:0;text-align:center">Watch<br>Video</p>
                        </div>
                        <video class="video-responsive product-additional-graphic" style="max-width:352px">
                            <source src="/vid/v_plenum.mp4" type="video/mp4">
                        </video>
                    </div>
                    <div class="col-lg-5 col-sm-6">
                        <h3>The <strong>only</strong> true air-supported conveyor</h3>
                        <p>Unlike other air-supported conveyors, whose inconsistent airflows create belt drag and load agitation, 
                            the Grisley ASC guarantees <strong>superior, consistent performance</strong> at a <strong>lower cost</strong>. 
                            The Grisley ASC is a sound investment because it takes the expense out of the conveying process.</p>
                        <p class="contact-us"><a href="/contact.html">Contact us today for a quote <span class="glyphicon glyphicon-chevron-right"></span></a></p>
                    </div>
                </div>
            </section>

        </div>
        
        <?php
            $footerTemplate = new Template("$root/php/html_templates/footer.php");
            echo $footerTemplate;
        ?>
    </body>
</html>
