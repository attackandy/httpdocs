<!DOCTYPE html>
<html>
    <head>
        <?php
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            include("$root/php/html_templates/template.php");
            
            $headContentTemplate = new Template("$root/php/html_templates/head_content.php");
            $headContentTemplate->title = "About Us";
            echo $headContentTemplate;
        ?>
    </head>
    <body id="body-about" class="body-background">
        
        <?php
            $loadingScreenTemplate = new Template("$root/php/html_templates/loading_screen.php");
            echo $loadingScreenTemplate;
        ?>
        
        <?php
            $headerTemplate = new Template("$root/php/html_templates/header.php");
            $headerTemplate->aboutActive = true;
            echo $headerTemplate;
        ?>
        
        <div id="content" class="container-fluid">
            
            <section id="about-header-wrapper" class="pitch-wrapper flexbox-center-vertically">
                <div id="about-header" class="pitch">
                    <div class="row row-top-buffer-small row-bottom-buffer-small">
                        <div class="col-xs-12 relative">
                            <img class="pitch-grisley-g" alt="Grisley" src="/img/grisley_g.png">
                            <div class="pitch-tagline-wrapper center-absolute">
                                <h1 class="pitch-tagline">About Us</h1>
                                <h2 class="pitch-tagline-sub">What makes Grisley the world-wide leader in conveyor technology?</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        
            <div id="about-sections">
                <section class="about-section">
                    <div class="row row-top-buffer">
                        <div class="col-lg-offset-1 col-lg-5 col-md-6">
                            <h3>Over 25 years of <strong>excellence</strong></h3>
                            <p>At Grisley ASC, we have dedicated over 25 years solely to developing and building air-supported conveyors. 
                                This has allowed us to provide superior design and engineering, better construction, lower costs, and the best customer service. 
                                With their superior performance, clean, quiet and safe operation, and energy savings, Grisley Air-Supported 
                                Conveyors are quickly rewriting industry standards for smooth, reliable, cost-effective conveying.</p>
                        </div>
                        <div class="col-lg-5 col-md-6 left-divider left-divider-disable-sm">
                            <h3>Trusted <strong>world-wide</strong> for bulk solids handling</h3>
                            <p>Grisley ASC designs and builds air-supported conveyors ideal for almost any project, including the transportation
                                of coal, grains, cement, salt, sand, and sugar. Our conveyors use a thin cushion of air, rather than idlers or 
                                troughing rollers, to support the conveyor belt. Eliminating mechanical friction, the Grisley ASC conveyor is 
                                the smoothest, most reliable, most cost-effective belt conveyor on the market.</p>
                        </div>
                    </div>
                    <div class="row row-top-buffer-small row-bottom-buffer flexbox-center-both hidden-xs">
                        <a href="/products/products.html"><button class="btn-black about-navigation-button">View our products</button></a>
                        <span class="about-navigation-separator">or</span>
                        <a href="/contact.html"><button class="btn-black about-navigation-button">Contact us</button></a>
                    </div>
                    <div class="hidden-sm hidden-md hidden-lg">
                        <div class="row row-top-buffer-small flexbox-center-both">
                            <a href="/products/products.html"><button class="btn-black about-navigation-button">View our products</button></a>
                        </div>
                        <div class="row flexbox-center-both">
                            <span class="about-navigation-separator">or</span>
                        </div>
                        <div class="row row-bottom-buffer flexbox-center-both">
                            <a href="/contact.html"><button class="btn-black about-navigation-button">Contact us</button></a>
                        </div>
                    </div>
                </section>
            </div>
                
        </div>
        
        <?php
            $footerTemplate = new Template("$root/php/html_templates/footer.php");
            echo $footerTemplate;
        ?>
    </body>
</html>

