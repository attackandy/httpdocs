<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            include("$root/php/html_templates/template.php");
            
            $headContentTemplate = new Template("$root/php/html_templates/head_content.php");
            $headContentTemplate->title = "Drafting & Design";
            echo $headContentTemplate;
        ?>
    </head>
    <body id="body-products">
		<?php include_once("analyticstracking.php") ?>
        
        <?php
            $loadingScreenTemplate = new Template("$root/php/html_templates/loading_screen.php");
            echo $loadingScreenTemplate;
        ?>
        
        <?php
            $headerTemplate = new Template("$root/php/html_templates/header.php");
            $headerTemplate->projectsActive = true;
            echo $headerTemplate;
        ?>
        
        <div id="content" class="container-fluid">
            
            <?php
                $brochureRibbonTemplate = new Template("$root/php/html_templates/brochure_ribbon.php");
                echo $brochureRibbonTemplate;
            ?>
            
            <section id="products-header-wrapper" class="pitch-wrapper flexbox-center-vertically">
                <div id="products-header" class="pitch">
                    <div class="row row-top-buffer-small row-bottom-buffer-small">
                        <div class="col-xs-12 relative">
                            <img class="pitch-grisley-g" alt="Grisley" src="/img/grisley_g.png">
                            <div class="pitch-tagline-wrapper center-absolute">
                                <h1 class="pitch-tagline">Projects</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        
            <section id="projects-section">
                <div class="row row-top-buffer row-bottom-buffer flexbox-center-vertically flexbox-center-vertically-disable-xs">
                    <figure class="col-lg-offset-1 col-lg-5 col-sm-6">
                        <img alt="Industrial Applications" src="/img/manufacturing-facility.jpg" class="product-image img-responsive">
                        <figcaption>Grisley Air-Supported Conveyors are fabricated to (CEMA) standards and have been produced out of various materials from carbon steel to various grades of stainless steel to aluminum depending upon the application. </figcaption>
                    </figure>
                    <div class="col-lg-5 col-sm-6">
                        <h2 class="product-name">Industrial Applications</h2>
                        <p class="product-description"><strong>Grisley Air-Supported Conveyors</strong> has provided bulk material handling solutions for the past three decades. Ask us for our complete project list of various industrial projects.</p>
                        <h3>The following is a list of various industrial applications:</h3>
                        <ul class="ul-no-margin col-lg-3 col-xs-6">
                            <li>Grain</li>
                            <li>Coal</li>
                            <li>Clinker</li>
                        </ul>
                        <ul class="ul-no-margin col-lg-3 col-xs-6">
                            <li>Cement</li>
                            <li>Oil-Shale</li>
                            <li>Gypsum</li>
                        </ul>
                        <ul class="ul-no-margin col-lg-3 col-xs-6">
                            <li>Meal</li>
                            <li>Malt</li>
                            <li>Salt</li>
                        </ul>
                        <ul class="ul-no-margin col-lg-3 col-xs-6">
                            <li>Garlic</li>
                            <li>Fruit</li>
                            <li>Produce</li>
                        </ul>
                    </div>
                </div>
            </section>

            <section id="home-projects-section" class="home-section-additional">
                <div class="row">
                    <div class="home-header-wrapper col-md-offset-2 col-md-8">
                        <h2>Grain Handling Projects</h2>
                        <div id="home-projects-header" class="home-header">
                            <p><strong>Grisley Air-Supported Conveyors</strong> have provided bulk material handling solutions for the past three decades. Ask us for our complete list of grain handling projects.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="projects-carousel">
                        <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                           data-toggle="modal" data-title="GRAIN HANDLING PROJECT - CHINA (1600 MTPH)" data-caption="" 
                           data-image="/img/grain/Grain1.jpg" data-target="#image-gallery">
                            <figure class="carousel-figure">
                                <img class="img-responsive" src="/img/grain/Grain1.jpg">
                                <figcaption class="carousel-figcaption">GRAIN HANDLING PROJECT<br>CHINA (1600 MTPH)</figcaption>
                            </figure>
                        </a>
                        <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                           data-toggle="modal" data-title="GRAIN HANDLING PROJECT - CHINA (1600 MTPH)" data-caption="" 
                           data-image="/img/grain/Grain2.jpg" data-target="#image-gallery">
                            <figure class="carousel-figure">
                                <img class="img-responsive" src="/img/grain/Grain2.jpg">
                                <figcaption class="carousel-figcaption">GRAIN HANDLING PROJECT<br>CHINA (1600 MTPH)</figcaption>
                            </figure>
                        </a>
                        <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                           data-toggle="modal" data-title="GRAIN HANDLING PROJECT - SOUTH EAST REGION (1800 MTPH)" data-caption="" 
                           data-image="/img/grain/Grain3.jpg" data-target="#image-gallery">
                            <figure class="carousel-figure">
                                <img class="img-responsive" src="/img/grain/Grain3.jpg">
                                <figcaption class="carousel-figcaption">GRAIN HANDLING PROJECT<br>SOUTH EAST REGION (1800 MTPH)</figcaption>
                            </figure>
                        </a>
                        <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                           data-toggle="modal" data-title="GRAIN HANDLING PROJECT - SOUTH EAST REGION (1800 MTPH)" data-caption="" 
                           data-image="/img/grain/Grain4.jpg" data-target="#image-gallery">
                            <figure class="carousel-figure">
                                <img class="img-responsive" src="/img/grain/Grain4.jpg">
                                <figcaption class="carousel-figcaption">GRAIN HANDLING PROJECT<br>SOUTH EAST REGION (1800 MTPH)</figcaption>
                            </figure>
                        </a>
                    </div>
                </div>
            </section>

            <section id="projects-section" class="home-section-additional">
                <div class="row">
                    <div class="home-header-wrapper col-md-offset-2 col-md-8">
                        <h2>Coal Handling Projects</h2>
                        <div id="home-projects-header" class="home-header">
                            <p><strong>Grisley Air-Supported Conveyors</strong> have provided bulk material handling solutions for the past three decades. Ask us for our complete list of coal handling projects.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="projects-carousel">
                        <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                           data-toggle="modal" data-title="COAL HANDLING PROJECT - SOUTH KOREA (2000 MTPH)" data-caption="" 
                           data-image="/img/coal/Coal1.jpg" data-target="#image-gallery">
                            <figure class="carousel-figure">
                                <img class="img-responsive" src="/img/coal/Coal1.jpg">
                                <figcaption class="carousel-figcaption">COAL HANDLING PROJECT<br>SOUTH KOREA (2000 MTPH)</figcaption>
                            </figure>
                        </a>
                        <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                           data-toggle="modal" data-title="COAL HANDLING PROJECT - SOUTHWEST REGION (1500 MTPH)" data-caption="" 
                           data-image="/img/coal/Coal2.jpg" data-target="#image-gallery">
                            <figure class="carousel-figure">
                                <img class="img-responsive" src="/img/coal/Coal2.jpg">
                                <figcaption class="carousel-figcaption">COAL HANDLING PROJECT<br>SOUTHWEST (1500 MTPH)</figcaption>
                            </figure>
                        </a>
                        <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                           data-toggle="modal" data-title="COAL HANDLING PROJECT - SOUTHWEST REGION (800 MTPH)" data-caption="" 
                           data-image="/img/coal/Coal3.jpg" data-target="#image-gallery">
                            <figure class="carousel-figure">
                                <img class="img-responsive" src="/img/coal/Coal3.jpg">
                                <figcaption class="carousel-figcaption">COAL HANDLING PROJECT<br>SOUTHWEST REGION (800 MTPH)</figcaption>
                            </figure>   
                        </a>
                    </div>
                </div>
            </section>

            <section id="home-projects-section" class="home-section-additional bottom-section-buffer">
                <div class="row">
                    <div class="home-header-wrapper col-md-offset-2 col-md-8">
                        <h2>Retrofit Projects</h2>
                        <div id="home-projects-header" class="home-header">
                            <p><strong>Grisley Air-Supported Conveyors</strong> have provided bulk material handling solutions for the past three decades. Ask us for our complete list of retrofit projects. - Our modular retrofit installation easily takes the place of conventional troughing idllers which eliminates roller maintenance as well as provides a significant health and safety increase of work area environment. This system eliminates the need for expensive baghouse costs and process air ducting which has eliminated air-modification permitting.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="projects-carousel">
                        <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                           data-toggle="modal" data-title="CEMENT HANDLING RETROFIT - HIGH ROCKIES (500 MTPH)" data-caption="" 
                           data-image="/img/retrofit/Retrofit1.jpg" data-target="#image-gallery">
                            <figure class="carousel-figure">
                                <img class="img-responsive" src="/img/retrofit/Retrofit1.jpg">
                                <figcaption class="carousel-figcaption">CEMENT HANDLING RETROFIT<br>HIGH ROCKIES (500 MTPH)</figcaption>
                            </figure>
                        </a>
                        <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                           data-toggle="modal" data-title="LOADING ZONE RETROFIT - MIDWEST REGION (4000 MTPH)" data-caption="" 
                           data-image="/img/retrofit/Retrofit2.jpg" data-target="#image-gallery">
                            <figure class="carousel-figure">
                                <img class="img-responsive" src="/img/retrofit/Retrofit2.jpg">
                                <figcaption class="carousel-figcaption">LOADING ZONE RETROFIT<br>MIDWEST REGION (4000 MTPH)</figcaption>
                            </figure>
                        </a>
                        <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                           data-toggle="modal" data-title="COAL HANDLING TUNNEL RETROFIT - MIDWEST REGION (1500 MTPH)" data-caption="" 
                           data-image="/img/retrofit/Retrofit3.jpg" data-target="#image-gallery">
                            <figure class="carousel-figure">
                                <img class="img-responsive" src="/img/retrofit/Retrofit3.jpg">
                                <figcaption class="carousel-figcaption">COAL HANDLING TUNNEL RETROFIT<br>MIDWEST REGION (1500 MTPH)</figcaption>
                            </figure>
                        </a>
                    </div>
                </div>
            </section>
                
        </div>
        
        <?php
            $footerTemplate = new Template("$root/php/html_templates/footer.php");
            echo $footerTemplate;
        ?>
    </body>
</html>
