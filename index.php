<!DOCTYPE html> 
<html>
    <head>
        <?php
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            include("$root/php/html_templates/template.php");
            
            $headContentTemplate = new Template("$root/php/html_templates/head_content.php");
            $headContentTemplate->title = "Grisley ASC - Air Supported Conveyors";
            echo $headContentTemplate;
        ?>
    </head>
    <body id="body-home" class="body-background">
      <?php include_once("analyticstracking.php") ?>

        
        <?php
            $loadingScreenTemplate = new Template("$root/php/html_templates/loading_screen.php");
            echo $loadingScreenTemplate;
        ?>
        
        <?php
            $headerTemplate = new Template("$root/php/html_templates/header.php");
            $headerTemplate->homeActive = true;
            echo $headerTemplate;
        ?>
        
        <div id="content" class="container-fluid">
            
            <?php
                $brochureRibbonTemplate = new Template("$root/php/html_templates/brochure_ribbon.php");
                echo $brochureRibbonTemplate;
            ?>
            
            <div class="row" style="position:relative">

                <section id="home-pitch-wrapper" class="pitch-wrapper flexbox-center-vertically">
                    <div id="home-pitch" class="pitch">
                        <div class="row pitch1">
                            <div class="col-lg-offset-2 col-lg-8 relative" style="height:100%">
                                <img class="pitch-grisley-g img-responsive center-absolute" alt="Grisley" src="/img/grisley_g.png">
                                <div class="pitch-tagline-wrapper center-absolute">
                                    <h1 class="pitch-tagline">Fully Contained Bulk Material Conveying<br>Dust Controlled Solutions</h1>
                                    <h2 class="pitch-tagline-sub">Whatever your conveying needs, Grisley provides <span class="italic">your solution</span>.</h2>
                                </div>
                            </div>
                        </div>

                        <div class="row pitch3">
                            <div class="flexbox-center-both" style="height:100%">
                                <button id="home-pitch-button-why" type="button">Why Grisley?</button>
                            </div>
                        </div>
                    </div>
                </section>
                
                <div id="home-sections-additional">

                    <section id="home-advantages-section" class="home-section-additional">
                        <div class="row">
                            <div class="home-header-wrapper col-md-offset-2 col-md-8">
                                <h2 class="home-header-title">Advantages</h2>
                                <div id="home-advantage-header" class="home-header">
                                    <h3>Learn how our ASC technology renders traditional conveying methods obsolete.</h3>
                                </div>
                            </div>
                        </div>
                        <div id="advantage-icons" class="not-visible">
                            <div class="row row-top-buffer-small">
                                <div id="advantage-icon-wrapper-environment" class="advantage-icon-wrapper col-xs-12 col-sm-4 col-lg-2" data-modal="modal-advantage-environment">
                                    <div id="advantage-icon-environment" class="relative">
                                        <i class="advantage-icon-circle fa fa-circle-thin center-absolute"></i>
                                        <i class="advantage-icon fa fa-leaf"></i>
                                    </div>
                                    <div id="advantage-summary-environment" class="advantage-summary-wrapper">
                                        <h4>Our Environment</h4>
                                        <p>Grisley’s reliable and cost-effective air-supported 
                                            conveyors are the environmentally-sound alternative.</p>
                                        <p class="advantage-summary-more">Click to read more <span class="glyphicon glyphicon-chevron-right"></span></p>
                                    </div>
                                </div>
                                <div id="advantage-icon-wrapper-cost" class="advantage-icon-wrapper col-xs-12 col-sm-4 col-lg-2" data-modal="modal-advantage-cost">
                                    <div id="advantage-icon-cost" class="relative">
                                        <i class="advantage-icon-circle fa fa-circle-thin center-absolute"></i>
                                        <i class="advantage-icon fa fa-usd"></i>
                                    </div>
                                    <div id="advantage-summary-cost" class="advantage-summary-wrapper">
                                        <h4>Lower Operating Cost</h4>
                                        <p>Save millions of dollars annually by eliminating the need for rollers.</p>
                                        <p class="advantage-summary-more">Click to read more <span class="glyphicon glyphicon-chevron-right"></span></p>
                                    </div>
                                </div>
                                <div id="advantage-icon-wrapper-power" class="advantage-icon-wrapper col-xs-12 col-sm-4 col-lg-2" data-modal="modal-advantage-power">
                                    <div id="advantage-icon-power" class="relative">
                                        <i class="advantage-icon-circle fa fa-circle-thin center-absolute"></i>
                                        <i class="advantage-icon fa fa-plug"></i>
                                    </div>
                                    <div id="advantage-summary-power" class="advantage-summary-wrapper">
                                        <h4>Lower Power</h4>
                                        <p>Save up to 35% on horizontal applications.</p>
                                        <p class="advantage-summary-more">Click to read more <span class="glyphicon glyphicon-chevron-right"></span></p>
                                    </div>
                                </div>
                                <div class="clearfix visible-sm"></div>
                                <div class="clearfix visible-md"></div>
                                <div id="advantage-icon-wrapper-maintenance" class="advantage-icon-wrapper col-xs-12 col-sm-4 col-lg-2" data-modal="modal-advantage-maintenance">
                                    <div id="advantage-icon-maintenance" class="relative">
                                        <i class="advantage-icon-circle fa fa-circle-thin center-absolute"></i>
                                        <i class="advantage-icon fa fa-wrench"></i>
                                    </div>
                                    <div id="advantage-summary-maintenance" class="advantage-summary-wrapper">
                                        <h4>Less Maintenance</h4>
                                        <p>Simplified operation greatly reduces the need for repair and part replacement.</p>
                                        <p class="advantage-summary-more">Click to read more <span class="glyphicon glyphicon-chevron-right"></span></p>
                                    </div>
                                </div>
                                <div id="advantage-icon-wrapper-volume" class="advantage-icon-wrapper col-xs-12 col-sm-4 col-lg-2" data-modal="modal-advantage-volume">
                                    <div id="advantage-icon-volume" class="relative">
                                        <i class="advantage-icon-circle fa fa-circle-thin center-absolute"></i>
                                        <i class="advantage-icon fa fa-volume-down"></i>
                                    </div>
                                    <div id="advantage-summary-volume" class="advantage-summary-wrapper">
                                        <h4>Quieter Operation</h4>
                                        <p>Increase workplace safety and reduce health hazards with air-supported conveying.</p>
                                        <p class="advantage-summary-more">Click to read more <span class="glyphicon glyphicon-chevron-right"></span></p>
                                    </div>
                                </div>
                                <div id="advantage-icon-wrapper-performance" class="advantage-icon-wrapper col-xs-12 col-sm-4 col-lg-2" data-modal="modal-advantage-performance">
                                    <div id="advantage-icon-performance" class="relative">
                                        <i class="advantage-icon-circle fa fa-circle-thin center-absolute"></i>
                                        <i class="advantage-icon fa fa-plus"></i>
                                    </div>
                                    <div id="advantage-summary-performance" class="advantage-summary-wrapper">
                                        <h4>Better Performance</h4>
                                        <p>Convey faster, farther, and at steeper angles with self-supporting architecture.</p>
                                        <p class="advantage-summary-more">Click to read more <span class="glyphicon glyphicon-chevron-right"></span></p>
                                    </div>
                                </div>
                                <div class="clearfix visible-sm"></div>
                                <div class="clearfix visible-md"></div>
                            </div>
                        </div>
                    </section>
                    <!--
                    <section id="home-projects-section" class="home-section-additional">
                        <div class="row">
                            <div class="home-header-wrapper col-md-offset-2 col-md-8">
                                <h2 class="home-header-title">Projects</h2>
                                <div id="home-projects-header" class="home-header">
                                    <h3>See our conveyors in the field</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="projects-carousel">
                                <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                                   data-toggle="modal" data-title="This is my title" data-caption="" 
                                   data-image="/img/carousel/1.jpg" data-target="#image-gallery">
                                    <img class="img-responsive" src="/img/carousel/1.jpg">
                                </a>
                                <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                                   data-toggle="modal" data-title="This is my title" data-caption="" 
                                   data-image="/img/carousel/2.jpg" data-target="#image-gallery">
                                    <img class="img-responsive" src="/img/carousel/2.jpg">
                                </a>
                                <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                                   data-toggle="modal" data-title="This is my title" data-caption="" 
                                   data-image="/img/carousel/3.jpg" data-target="#image-gallery">
                                    <img class="img-responsive" src="/img/carousel/3.jpg">
                                </a>
                                <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                                   data-toggle="modal" data-title="This is my title" data-caption="" 
                                   data-image="/img/carousel/4.jpg" data-target="#image-gallery">
                                    <img class="img-responsive" src="/img/carousel/4.jpg">
                                </a>
                                <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                                   data-toggle="modal" data-title="This is my title" data-caption="" 
                                   data-image="/img/carousel/5.jpg" data-target="#image-gallery">
                                    <img class="img-responsive" src="/img/carousel/5.jpg">
                                </a>
                                <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                                   data-toggle="modal" data-title="This is my title" data-caption="" 
                                   data-image="/img/carousel/6.jpg" data-target="#image-gallery">
                                    <img class="img-responsive" src="/img/carousel/6.jpg">
                                </a>
                                <a class="carousel-image-wrapper thumbnail" href="#" data-image-id="" 
                                   data-toggle="modal" data-title="This is my title" data-caption="" 
                                   data-image="/img/carousel/7.jpg" data-target="#image-gallery">
                                    <img class="img-responsive" src="/img/carousel/7.jpg">
                                </a>
                            </div>
                        </div>
                    </section>
                    
                    <section id="home-contact-section" class="home-section-additional">
                        <div class="row row-top-buffer not-visible">
                            <div id="contact-form-wrapper" class="col-xs-12 col-md-offset-1 col-md-10 col-lg-offset-3 col-lg-6">
                                <?php
                                    $contactFormTemplate = new Template("$root/php/html_templates/contact_form.php");
                                    $contactFormTemplate->contactFormSubHeader = "or <a href='/contact.php'>visit our contact page</a>";
                                    echo $contactFormTemplate;
                                ?>
                            </div>
                        </div>
                    </section>
                    -->
                </div>
            </div>
            
        </div>
        
        <?php
            $footerTemplate = new Template("$root/php/html_templates/footer.php");
            echo $footerTemplate;
        ?>

        <div class="modal modal-advantage animated slideInUp" id="modal-advantage-environment" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content relative">
                    <img class="advantage-text-grisley-g center-absolute" alt="Grisley" src="/img/grisley_g.png">
                    <div class="modal-header relative">
                        <button type="button" class="close" data-dismiss="modal"><img src="/img/close.svg"><span class="sr-only">Close</span></button>
                        <h3 class="modal-title"><i class="fa fa-leaf"></i><br>Our Environment</h3>
                    </div>
                    <hr>
                    <div class="modal-body">
                        <p>Serious environmental concerns play a central role in today’s bulk solids handling industries. 
                            The ecological, safety, and health hazards of product dust are now well-documented, and, recognizing this fact, 
                            OSHA, the EPA, MSHA and other governmental agencies vigorously regulate these industries. This is especially the 
                            case with hazardous materials, such as chemicals, cement, lime, gypsum, coal, coke, mineral ore, heavy metals, 
                            silica, and waste products.</p><br>
                        <p>Not surprisingly, many industry leaders are taking a hard look at their methods of conveying since conventional 
                            mechanical conveyors are dust-generating machines. No matter how well designed, conventional mechanical conveyors 
                            pose a serious environmental hazard. That hazard comes directly from the friction-intensive principles of their 
                            operation, principles that are both obsolete and ineffective. Grisley’s reliable and cost-effective air-supported 
                            conveyors are the environmentally-sound alternative. Taking the industry beyond roller conveying has introduced a 
                            number of unique environmental safety innovations that simply are not possible with conventional conveying methods.</p>
                    </div>
                    <button type="button" class="btn btn-success hidden-sm hidden-md hidden-lg close-button-secondary" data-dismiss="modal">Close<span class="sr-only">Close</span></button>
                </div>
            </div>
        </div>
        
        <div class="modal modal-advantage animated slideInUp" id="modal-advantage-cost" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content relative">
                    <img class="advantage-text-grisley-g center-absolute" alt="Grisley" src="/img/grisley_g.png">
                    <div class="modal-header relative">
                        <button type="button" class="close" data-dismiss="modal"><img src="/img/close.svg"><span class="sr-only">Close</span></button>
                        <h3 class="modal-title"><i class="fa fa-usd"></i><br>Lower Operating Cost</h3>
                    </div>
                    <hr>
                    <div class="modal-body">
                        <p>Conventional roller conveyors, which are the most common mode of handling bulk solids, rely on rollers to carry and 
                            track the belt. The friction between the rollers and the belt agitates the material it is carrying, which produces 
                            substantial product degradation and dust. Belt sag is another contributor to degradation and dust. Due to the combined 
                            forces of gravity and product bulk, roller conveyors cause their belts to sag between rollers, which causes the belt 
                            to move like a roller coaster. The problem gets progressively worse as the speed increases, effectively turning the 
                            conveyor into a product pulverizer. The extraordinary precautions the industry takes to protect roller bearing assemblies 
                            from the destructive effects of fugitive product dust demonstrates the fundamental flaw in their operating principle. 
                            Mining and mineral companies alone spend <strong>millions of dollars annually</strong> to repair and replace damaged rollers,  
                            whose bearings are damaged from dust penetration. To address this problem, many conveyor manufacturers have gone to great 
                            lengths to build better rollers and protected bearing assemblies. But they are spending their time perfecting an 
                            inherently flawed technology. The most practical solution is to eliminate the need for roller-based conveying, 
                            not to make it better.</p>
                    </div>
                    <button type="button" class="btn btn-success hidden-sm hidden-md hidden-lg close-button-secondary" data-dismiss="modal">Close<span class="sr-only">Close</span></button>
                </div>
            </div>
        </div>
        
        <div class="modal modal-advantage animated slideInUp" id="modal-advantage-power" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content relative">
                    <img class="advantage-text-grisley-g center-absolute" alt="Grisley" src="/img/grisley_g.png">
                    <div class="modal-header relative">
                        <button type="button" class="close" data-dismiss="modal"><img src="/img/close.svg"><span class="sr-only">Close</span></button>
                        <h3 class="modal-title"><i class="fa fa-plug"></i><br>Lower Power</h3>
                    </div>
                    <hr>
                    <div class="modal-body">
                        <p>Grisley ASC conveyors use a low-horsepower centrifugal fan to provide the airflow that supports the belt. A single fan 
                            supports up to 600 feet of conveyor. The Grisley ASC requires substantially less horsepower to operate, <strong>saving 
                            up to 35%</strong> on horizontal applications.</p>
                    </div>
                    <button type="button" class="btn btn-success hidden-sm hidden-md hidden-lg close-button-secondary" data-dismiss="modal">Close<span class="sr-only">Close</span></button>
                </div>
            </div>
        </div>
        
        <div class="modal modal-advantage animated slideInUp" id="modal-advantage-maintenance" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content relative">
                    <img class="advantage-text-grisley-g center-absolute" alt="Grisley" src="/img/grisley_g.png">
                    <div class="modal-header relative">
                        <button type="button" class="close" data-dismiss="modal"><img src="/img/close.svg"><span class="sr-only">Close</span></button>
                        <h3 class="modal-title"><i class="fa fa-wrench"></i><br>Less Maintenance</h3>
                    </div>
                    <hr>
                    <div class="modal-body">
                        <p>The airflow completely eliminates the belt sag inherent in conventional troughing conveyors. The Grisley air 
                            supported conveyor maintains a consistent level of support throughout every square inch of the belt’s movement. 
                            The smooth belt motion keeps the product stationary, eliminating the source of product degradation. No more 
                            troughing rollers to repair and replace: Since the belt is free of roller support, the Grisley ASC conveyor is 
                            dramatically less susceptible to the hazards of dust inherent in conventional conveyors. Without rollers, the 
                            Grisley conveyor reduces maintenance to the head and tail sections, which simplifies its operation.</p>
                    </div>
                    <button type="button" class="btn btn-success hidden-sm hidden-md hidden-lg close-button-secondary" data-dismiss="modal">Close<span class="sr-only">Close</span></button>
                </div>
            </div>
        </div>
        
        <div class="modal modal-advantage animated slideInUp" id="modal-advantage-volume" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content relative">
                    <img class="advantage-text-grisley-g center-absolute" alt="Grisley" src="/img/grisley_g.png">
                    <div class="modal-header relative">
                        <button type="button" class="close" data-dismiss="modal"><img src="/img/close.svg"><span class="sr-only">Close</span></button>
                        <h3 class="modal-title"><i class="fa fa-volume-down"></i><br>Quieter Operation</h3>
                    </div>
                    <hr>
                    <div class="modal-body">
                        <p>Due to its full enclosure, the lower horsepower of its drive, and the absence of load agitation, the Grisley Air 
                            Supported Conveyor is much quieter in normal operation. This benefit makes a positive contribution to workplace 
                            safety, since mechanical noise increases the likelihood of accidents and long-term employee hearing loss.</p>
                    </div>
                    <button type="button" class="btn btn-success hidden-sm hidden-md hidden-lg close-button-secondary" data-dismiss="modal">Close<span class="sr-only">Close</span></button>
                </div>
            </div>
        </div>
        
        <div class="modal modal-advantage animated slideInUp" id="modal-advantage-performance" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content relative">
                    <img class="advantage-text-grisley-g center-absolute" alt="Grisley" src="/img/grisley_g.png">
                    <div class="modal-header relative">
                        <button type="button" class="close" data-dismiss="modal"><img src="/img/close.svg"><span class="sr-only">Close</span></button>
                        <h3 class="modal-title"><i class="fa fa-plus"></i><br>Better Performance</h3>
                    </div>
                    <hr>
                    <div class="modal-body">
                        <p>Reliable air support allows higher-volume, faster conveying at angles <strong>up to 25 degrees</strong>. 
                            Compare the effectiveness to conventional conveyors, which can only handle angles up to 16 degrees.</p><br>
                        <p>Sound plenum architecture self-supports conveyor spans <strong>up to 45 feet</strong>, reducing the amount of 
                            truss-work and structural support.</p><br>
                        <p>Airtight and weather-resistant covers protect delicate loads inside and prevent pollution outside the conveyor. 
                            The precisely-calibrated airflow eliminates bumps and roller friction from the conveying process, eliminating 
                            the source of dust. This method of dust prevention is so effective that the Grisley conveyor can be enclosed 
                            fully and safely with airtight and weather-resistant covers. Together with its safe dust-tight enclosure, 
                            the air-supported conveyor’s smooth belt motion sets a new standard in dust control.</p><br>
                        <p>With conventional conveying, on the other hand, the user must factor the costs of maintaining, cleaning up, and 
                            replacing damaged product; all costs that are incurred from the conveyor’s operation. With their superior performance, 
                            lower sales and operating costs, and professional engineering support, Grisley Air Supported Conveyors set new standards 
                            for smooth, reliable, cost-effective conveying.</p>
                    </div>
                    <button type="button" class="btn btn-success hidden-sm hidden-md hidden-lg close-button-secondary" data-dismiss="modal">Close<span class="sr-only">Close</span></button>
                </div>
            </div>
        </div>
        
        <div class="modal modal-advantage animated slideInUp" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content relative">
                    <div class="modal-header relative">
                        <button type="button" class="close" data-dismiss="modal"><img src="/img/close.svg"><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="image-gallery-title"></h4>
                    </div>
                    <div class="modal-body">
                        <img id="image-gallery-image" class="img-responsive" src="" />
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-8 text-justify" id="image-gallery-caption"></div>
                        <button type="button" class="btn btn-success hidden-sm hidden-md hidden-lg close-button-secondary" data-dismiss="modal">Close<span class="sr-only">Close</span></button>
                    </div>
                </div>
            </div>
        </div>
        
    </body>
</html>
