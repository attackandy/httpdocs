<!DOCTYPE html>
<html>
    <head>
        <?php
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            include("$root/php/html_templates/template.php");
            
            $headContentTemplate = new Template("$root/php/html_templates/head_content.php");
            $headContentTemplate->title = "Contact Us";
            echo $headContentTemplate;
        ?>
    </head>
    <body id="body-contact" class="body-background">
		<?php include_once("analyticstracking.php") ?>
        
        <?php
            $loadingScreenTemplate = new Template("$root/php/html_templates/loading_screen.php");
            echo $loadingScreenTemplate;
        ?>
        
        <?php
            $headerTemplate = new Template("$root/php/html_templates/header.php");
            $headerTemplate->contactActive = true;
            echo $headerTemplate;
        ?>
        
        <div id="content" class="container-fluid">
            
            <?php
                $brochureRibbonTemplate = new Template("$root/php/html_templates/brochure_ribbon.php");
                echo $brochureRibbonTemplate;
            ?>
            
            <section id="contact-section">
                
                <div class="row row-top-buffer-small row-bottom-buffer">
                    <div id="additional-contact-info" class="col-xs-12 col-md-offset-1 col-md-3 col-md-push-7">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-12 additional-contact-info-item">
                                <h4 class="flexbox-center-vertically"><i class="fa fa-map-marker additional-contact-info-item-icon"></i>Locations</h4>
                                <p>Salt Lake City, Utah</p>
								<p>Denver, colorado</p>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-12 additional-contact-info-item">
                                <h4 class="flexbox-center-vertically"><i class="fa fa-envelope additional-contact-info-item-icon"></i>E-mail</h4>
                                <p><a id="footer-contact-link-mail" href="mailto:info@grisley.com">info@grisley.com</a></p>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-12 additional-contact-info-item">
                                <h4 class="flexbox-center-vertically"><i class="fa fa-phone-square additional-contact-info-item-icon"></i>Phone</h4>
								<p><a href="tel:13037566474">+1 303-756-6474</a></p>
                            </div>

                        </div>
                    </div>
                    <div id="contact-form-wrapper" class="col-xs-12 col-md-7 col-md-pull-3">
                        <?php
                            $contactFormTemplate = new Template("$root/php/html_templates/contact_form.php");
                            $contactFormTemplate->contactFormSubHeader = "Request a quote, leave a comment, or ask a question";
                            echo $contactFormTemplate;
                        ?>
                    </div>
                </div>
                
            </section>

        </div>
        
        <?php
            $footerTemplate = new Template("$root/php/html_templates/footer.php");
            echo $footerTemplate;
        ?>
    </body>
</html>

