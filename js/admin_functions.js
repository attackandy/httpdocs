function OnAddNewCategoryClick()
{
    $("#modal-category-add").modal({show: true});
}

function ValidateCategoryName(categoryName)
{
    return (categoryName && categoryName.trim() !== "" && 
        categoryName.match(/^[0-9a-zA-Z\s]+$/));
}

function OnNewCategorySubmit(dbId)
{
    try
    {
        var categoryName = $("#add-category-name").val();
        if (ValidateCategoryName(categoryName))
        {
            $.ajax({
                type: 'POST',
                url: "/php/add_category.php",
                data: { name: categoryName, dbId: dbId }
            })
            .done(function(responseData) {
                var responseObject = JSON.parse(responseData);
                $("#category").html(responseObject.categoryOptions);
                $("#categories-to-edit").html(responseObject.categoriesToEdit);
                $("#modal-category-add form")[0].reset();
                $("#modal-category-add").modal("hide");
            })
            .fail(function(xhr, status, error) {
                alert("Error adding new category.");
            });
        }
        else
        {
            alert("Category name must be only letters and numbers.");
        }
    }
    catch(err)
    {
        alert(err.message);
    }
    return false;
}

function UploadFile(file, index, dbId)
{
    var id = "upload-message-" + index;
    var p = document.createElement("p");
    
    p.innerHTML = "Uploading file '" + file.name + "'...";
    p.setAttribute("id", id);
    $("#modal-upload-progress").find(".modal-body").append(p);
    
    var data = new FormData();
    data.append("file", file);
    data.append("categoryId", parseInt($("#category").val()));
    data.append("dbId", dbId);
    $.ajax({
        type: 'POST',
        url: "/php/upload_document.php",
        processData: false,
        contentType: false,
        data: data
    })
    .done(function(responseData) {
        $("#delete-files tbody").html(responseData);
        $("#" + id).append("success!");
    })
    .fail(function(xhr, status, error) {
        $("#" + id).append("failed");
    });
}

function OnFileUploadSubmit(dbId)
{
    try
    {
        if ($('#category > option').length > 0)
        {
            $("#modal-upload-progress").find(".modal-body").html("");
            $("#modal-upload-progress").modal({show: true});
            var files = $("#file")[0].files;
            for (var i = 0; i < files.length; i++)
            {
                UploadFile(files[i], i, dbId);
            }
            
            $("#upload-form")[0].reset();
        }
        else
        {
            alert("A category must be added and selected before a file can be uploaded.");
        }
    }
    catch(err)
    {
        // If we don't catch errors, the form's onsubmit() will bypass
        // the "return false;" statement and refresh the page
        alert(err.message);
    }
    
    return false;
}

function OnCategoryRenameClick(id, dbId)
{
    var categoryId = id.match( /\d+/g )[0];
    $("#modal-category-rename").modal({show: true});
    $("#modal-category-rename form").submit(function(event) {
        event.preventDefault();
        OnRenameCategorySubmit(categoryId, dbId);
    });
}

function OnRenameCategorySubmit(categoryId, dbId)
{
    try
    {
        var categoryName = $("#rename-category-name").val();
        if (ValidateCategoryName(categoryName))
        {
            $.ajax({
                type: 'POST',
                url: "/php/rename_category.php",
                data: 
                { 
                    name: $("#rename-category-name").val(),
                    id: parseInt(categoryId),
                    dbId: dbId
                }
            })
            .done(function(responseData) {
                var responseObject = JSON.parse(responseData);
                $("#category").html(responseObject.categoryOptions);
                $("#categories-to-edit").html(responseObject.categoriesToEdit);
                $("#modal-category-rename form")[0].reset();
                $("#modal-category-rename").modal("hide");
            })
            .fail(function(xhr, status, error) {
                alert("Error renaming category.");
            });
        }
        else
        {
            alert("Category name must be only letters and numbers.");
        }
    }
    catch(err)
    {
        alert(err.message);
    }
    return false;
}

function OnCategoryDeleteClick(id, dbId)
{
    var categoryId = id.match( /\d+/g )[0];
    var categoryName = $("#" + id).closest("p").data("name");
    if (confirm("Delete category '" + categoryName + "'?"))
    {
        $.ajax({
            type: 'POST',
            url: "/php/delete_category.php",
            data: { id: parseInt(categoryId), dbId: dbId }
        })
        .done(function(responseData) {
            var responseObject = JSON.parse(responseData);
            $("#category").html(responseObject.categoryOptions);
            $("#categories-to-edit").html(responseObject.categoriesToEdit);
        })
        .fail(function(xhr, status, error) {
            alert("Error deleting category " +
                "(make sure to delete any files that use this category " +
                "before attempting to delete this category).");
        });
    }
}

function OnSelectAllFilesToDelete(element)
{
    $("#delete-files tbody").find("input[type=checkbox]").each(function () {
        this.checked = element.checked;
    });
}

function OnDeleteSelectedFilesClicked(dbId)
{
    var idsToDelete = [];
    $("#delete-files tbody").find("input[type=checkbox]").each(function () {
        if (this.checked)
        {
            var documentId = this.id.match( /\d+/g )[0];
            idsToDelete.push(parseInt(documentId));
        }
    });
    
    if (idsToDelete.length === 0)
    {
        alert("No file(s) selected");
    }
    else
    {
        if (confirm("Delete selected file(s)?"))
        {
            $.ajax({
                type: 'POST',
                url: "/php/delete_documents.php",
                data: { ids: JSON.stringify(idsToDelete), dbId: dbId }
                })
                .done(function(responseData) {
                    $("#delete-files tbody").html(responseData);
                })
                .fail(function(xhr, status, error) {
                    alert("Error deleting one or more files. " +
                        "Please refresh the page.");
            });
        }
    }
}

function OnBrochureStatisticsFormSubmit()
{
    var timeQuantity = $('#time-quantity').val();
    var timeUnits = $('#time-units').val();
    
    var sinceDateTime = new Date();
    
    switch (timeUnits)
    {
        case 'days':
            sinceDateTime.setDate(sinceDateTime.getDate() - timeQuantity);
            break;
        case 'weeks':
            sinceDateTime.setDate(sinceDateTime.getDate() - (timeQuantity * 7));
            break;
        case 'months':
            sinceDateTime.setMonth(sinceDateTime.getMonth() - timeQuantity);
            break;
        case 'years':
            sinceDateTime.setYear(sinceDateTime.getFullYear() - timeQuantity);
            break;
        default:
            alert("Error getting the brochure download count. Please ask Paul to fix it.");
            return;
    }
    
    var sinceTimestamp = sinceDateTime.toISOString().slice(0, 19).replace('T', ' ');
    
    $.ajax({
        type: 'POST',
        url: "/php/get_download_count.php",
        data: { item: 0, sinceTimestamp: sinceTimestamp }
        })
        .done(function(responseData) {
            $("#brochure-download-count").html(responseData + " downloads since " + sinceDateTime);
        })
        .fail(function(xhr, status, error) {
            alert("Error getting the brochure download count from the database. " +
                "Please refresh the page (and also ask Paul to fix it).");
    });
    
    return false;
}

$(document).ready(function() {
    $("#upload-form")[0].reset();
});

