<?php
    class Document
    {
        public $id = -1;
        public $name = "";
        public $path = "";
        
        function __construct($id, $path)
        {
            $this->id = $id;
            $this->name = basename($path);
            $this->path = $path;
        }
    }
?>

