<?php
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    require_once("$root/php/db_interface.php");
    require_once("$root/php/view.php");

    if(isset($_FILES['file']) and !$_FILES['file']['error'])
    {
        $uploadPath = $_FILES['file']['tmp_name'];
        $path = "/info/uploads/" . $_FILES['file']['name'];
        $categoryId = filter_input(INPUT_POST, 'categoryId');
        $dbId = filter_input(INPUT_POST, 'dbId');
        
        if (!file_exists($uploadPath))
        {
            throw new Exception("File does not exist in location '$uploadPath'");
        }
        
        move_uploaded_file($uploadPath, $root . $path);
        
        if (!file_exists($root . $path))
        {
            throw new Exception("File was not moved to location '$root " . "$path'");
        }
        
        $documents = DbInterface::insertFile($path, $categoryId, $dbId);
        
        $view = new View();
        echo $view->writeFilesToDelete($documents);
    }
?>

