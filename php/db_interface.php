<?php
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    require_once("$root/php/document_list.php");
    require_once("$root/php/category.php");
    require_once("$root/php/db_info.php");
    
    class DbInterface
    {
        private static function connect($dbId)
        {
            $dbUser = "";
            $dbName = "";
            
            switch ($dbId)
            {
                case 0:
                    $dbUser = "grisley1";
                    $dbName = "grisley_info";
                    break;
                case 1:
                    $dbUser = "grisley_acc1";
                    $dbName = "grisley_info_acc1";
                    break;
                case 2:
                    $dbUser = "grisley_acc2";
                    $dbName = "grisley_info_acc2";
                    break;
                case 3:
                    $dbUser = "grisley_acc3";
                    $dbName = "grisley_info_acc3";
                    break;
                case 4:
                    $dbUser = "grisley_acc4";
                    $dbName = "grisley_info_acc4";
                    break;
                case 5:
                    $dbUser = "grisley_acc5";
                    $dbName = "grisley_info_acc5";
                    break;
                default:
                    throw new Exception("Failed to connect to MySQL: " .
                        "Invalid database ID.");
            }
            
            $link = mysqli_connect("localhost", $dbUser, "61zqc6@T", $dbName);
    
            // Check connection
            if (mysqli_connect_errno())
            {
                throw new Exception("Failed to connect to MySQL: " .
                    mysqli_connect_error());
            }
            
            return $link;
        }
        
        private static function disconnect($link)
        {
            mysqli_close($link);
        }
        
        private static function getCategories($link)
        {
            $categories = array();
            
            $result = mysqli_query($link, "SELECT * FROM `categories`");
            if (!$result)
            {
                throw new Exception("Failed to get categories: " .
                     mysqli_error($link));
            }
            
            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
            {
                $category = new Category($row['Id'], $row['Name']);
                array_push($categories, $category);
            }
            
            return $categories;
        }
        
        private static function getDocumentsForCategories($link, $categories)
        {
            $documents = array();
            
            foreach ($categories as $category)
            {
                $categoryId = $category->id;

                $documentList = new DocumentList($categoryId, $category->name);

                $result = mysqli_query($link, 
                    "SELECT * FROM `documents` WHERE CategoryId=$categoryId");
                if (!$result)
                {
                    throw new Exception("Failed to get documents: " .
                         mysqli_error($link));
                }

                while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
                {
                    $documentList->addDocument($row['Id'], $row['Path']);
                }

                array_push($documents, $documentList);
            }
            
            return $documents;
        }
        
        public static function getInfoFromDatabase($dbId)
        {
            $link = self::connect($dbId);
            
            $categories = self::getCategories($link);
            $documents = self::getDocumentsForCategories($link, $categories);
            
            self::disconnect($link);
            
            return new DbInfo($documents, $categories);          
        }
        
        public static function insertCategory($categoryName, $dbId)
        {
            $link = self::connect($dbId);
            
            $categoryName = mysqli_real_escape_string($link, $categoryName);
            
            $query = "INSERT INTO `categories`(`Name`) VALUES ('$categoryName')";
            $result = mysqli_query($link, $query);
            if (!$result)
            {
                throw new Exception("Failed to execute query ($query): " .
                     mysqli_error($link));
            }
            
            $categories = self::getCategories($link);
            
            self::disconnect($link);
            
            return $categories;
        }
        
        public static function insertFile($path, $categoryId, $dbId)
        {
            $link = self::connect($dbId);
            
            $path = mysqli_real_escape_string($link, $path);
            $categoryId = intval($categoryId);
            if (!$categoryId)
            {
                throw new Exception("Failed to insert file into database " .
                    "(category ID must be an integer).");
            }
            
            $query = "INSERT INTO `documents`(`CategoryId`, `Path`) "
                . "VALUES ($categoryId, '$path')";
            $result = mysqli_query($link, $query);
            if (!$result)
            {
                throw new Exception("Failed to execute query ($query): " .
                     mysqli_error($link));
            }
            
            $categories = self::getCategories($link);
            $documents = self::getDocumentsForCategories($link, $categories);
            
            self::disconnect($link);
            
            return $documents;
        }
        
        public static function renameCategory($categoryId, $categoryName, $dbId)
        {
            $link = self::connect($dbId);
            
            $categoryName = mysqli_real_escape_string($link, $categoryName);
            $categoryId = intval($categoryId);
            if (!$categoryId)
            {
                throw new Exception("Failed to rename category " .
                    "(category ID must be an integer).");
            }
            
            $query = "UPDATE `categories` SET `Name` = '$categoryName' WHERE `categories`.`Id` = $categoryId";
            $result = mysqli_query($link, $query);
            if (!$result)
            {
                throw new Exception("Failed to execute query ($query): " .
                     mysqli_error($link));
            }
            
            $categories = self::getCategories($link);
            
            self::disconnect($link);
            
            return $categories;
        }
        
        public static function deleteCategory($categoryId, $dbId)
        {
            $link = self::connect($dbId);
            
            $categoryId = intval($categoryId);
            if (!$categoryId)
            {
                throw new Exception("Failed to delete category " .
                    "(category ID must be an integer).");
            }
            
            $query = "DELETE FROM `categories` WHERE `categories`.`Id` = $categoryId";
            $result = mysqli_query($link, $query);
            if (!$result)
            {
                throw new Exception("Failed to execute query ($query): " .
                     mysqli_error($link));
            }
            
            $categories = self::getCategories($link);
            
            self::disconnect($link);
            
            return $categories;
        }
        
        public static function deleteDocuments($documentIds, $dbId)
        {
            $link = self::connect($dbId);
            
            foreach ($documentIds as $documentId)
            {
                $documentId = intval($documentId);
                if (!$documentId)
                {
                    throw new Exception("Failed to delete document " .
                        "(document ID must be an integer).");
                }
                
                $query = "DELETE FROM `documents` WHERE `documents`.`Id` = $documentId";
                $result = mysqli_query($link, $query);
                if (!$result)
                {
                    throw new Exception("Failed to execute query ($query): " .
                         mysqli_error($link));
                }
            }
            
            $categories = self::getCategories($link);
            $documents = self::getDocumentsForCategories($link, $categories);
            
            self::disconnect($link);
            
            return $documents;
        }
        
        public static function downloadBrochure()
        {
            $link = self::connect(0);
            
            date_default_timezone_set("UTC");
            $timestamp = date('Y-m-d G:i:s');
            
            $query = "INSERT INTO `downloads`(`Item`, `Timestamp`) "
                . "VALUES (0, '$timestamp')";
            $result = mysqli_query($link, $query);
            if (!$result)
            {
                throw new Exception("Failed to execute query ($query): " .
                     mysqli_error($link));
            }
            
            self::disconnect($link);
        }
        
        public static function getDownloadCount($item, $sinceTimestamp)
        {
            $link = self::connect(0);
            
            $query = "SELECT * FROM `downloads` WHERE Timestamp > '$sinceTimestamp'  AND Item = $item";
            $result = mysqli_query($link, $query);
            if (!$result)
            {
                throw new Exception("Failed to execute query ($query): " .
                     mysqli_error($link));
            }
            
            self::disconnect($link);
            
            return $result->num_rows;
        }
    }
?>

