<!DOCTYPE html> 
<html>
    <head>
        <?php
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            require_once("$root/php/db_interface.php");
            require_once("$root/php/view.php");
            
            $info = DbInterface::getInfoFromDatabase($dbId);
            
            $view = new View();
            
            $headContentTemplate = new Template("$root/php/html_templates/head_content.php");
            $headContentTemplate->title = $title;
            echo $headContentTemplate;
        ?>
        
        <script src="/js/admin_functions.js"></script>
    </head>
    <body>
        <?php
            $adminBodyTemplate = new Template("$root/php/html_templates/admin_body.php");
            $adminBodyTemplate->title = $title;
            $adminBodyTemplate->view = $view;
            $adminBodyTemplate->info = $info;
            $adminBodyTemplate->dbId = $dbId;
            echo $adminBodyTemplate;
        ?>
    </body>
</html>

