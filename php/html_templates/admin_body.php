<div class="container">

    <div class="jumbotron">
        <h1><?php echo $title ?></h1>      
        <p>Add, remove, and edit site content.</p>
    </div>

    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#upload">Upload files</a>
                </h4>
            </div>
            <div id="upload" class="panel-collapse collapse in">
                <div class="panel-body">
                    <form role="form" id="upload-form" onsubmit="return OnFileUploadSubmit(<?php echo $dbId ?>)">
                        <div class="form-group">
                            <label for="category">Category:</label>
                            <select id="category" name="category">
                                <?php
                                    echo $view->writeCategoryOptions($info->categories);
                                ?>
                            </select>
                            <a href="#" onclick="OnAddNewCategoryClick()"> + Add new category</a>
                        </div>
                        <div class="form-group">
                            <input type="file" id="file" multiple required>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#edit-categories">Edit categories</a>
                </h4>
            </div>
            <div id="edit-categories" class="panel-collapse collapse">
                <div class="panel-body">
                    <div id="categories-to-edit">
                        <?php
                            echo $view->writeCategoriesToEdit($info->categories, $dbId);
                        ?>
                    </div>
                    <a href="#" onclick="OnAddNewCategoryClick()"> + Add new category</a>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#delete-files">Delete files</a>
                </h4>
            </div>
            <div id="delete-files" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="table-responsive">          
                        <table class="table">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" onclick="OnSelectAllFilesToDelete(this)" /></th>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Path</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    echo $view->writeFilesToDelete($info->documents);
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <button type="button" class="btn btn-danger" onclick="OnDeleteSelectedFilesClicked(<?php echo $dbId ?>)"><span class="glyphicon glyphicon-remove"></span> Delete Selected</button>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#view-statistics">View statistics</a>
                </h4>
            </div>
            <div id="view-statistics" class="panel-collapse collapse">
                <div class="panel-body">
                    <form role="form" id="brochure-form" onsubmit="return OnBrochureStatisticsFormSubmit()">
                        <div class="form-group">
                            <label for="time-quantity">View number of brochure downloads in the past </label>
                            <input id="time-quantity" name="time-quantity" type="number" min="1" max="365">
                            <select id="time-units" name="time-units">
                                <option value="days">days</option>
                                <option value="weeks">weeks</option>
                                <option value="months">months</option>
                                <option value="years">years</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                        <div id="brochure-download-count"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-category-add" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content relative">
            <div class="modal-header relative">
                <button type="button" class="close" data-dismiss="modal"><img src="/img/close.svg"><span class="sr-only">Close</span></button>
                <h3 class="modal-title">Add Category</h3>
            </div>
            <div class="modal-body">
                <form role="form" id="category-form" onsubmit="return OnNewCategorySubmit(<?php echo $dbId ?>)">
                    <div class="form-group">
                        <label for="add-category-name">New category name:</label>
                        <input type="text" class="form-control" id="add-category-name" name="add-category-name" autocomplete="off" required>
                    </div>
                    <button type="submit" class="btn btn-default">Add</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-category-rename" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content relative">
            <div class="modal-header relative">
                <button type="button" class="close" data-dismiss="modal"><img src="/img/close.svg"><span class="sr-only">Close</span></button>
                <h3 class="modal-title">Rename Category</h3>
            </div>
            <div class="modal-body">
                <form role="form" id="category-form">
                    <div class="form-group">
                        <label for="rename-category-name">New category name:</label>
                        <input type="text" class="form-control" id="rename-category-name" name="rename-category-name" autocomplete="off" required>
                    </div>
                    <button type="submit" class="btn btn-default">Apply</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-upload-progress" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content relative">
            <div class="modal-header relative">
                <button type="button" class="close" data-dismiss="modal"><img src="/img/close.svg"><span class="sr-only">Close</span></button>
                <h3 class="modal-title">File Upload Progress</h3>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
