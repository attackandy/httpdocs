<!DOCTYPE html>
<html>
    <head>
        <?php
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            require_once("$root/php/db_interface.php");
            require_once("$root/php/view.php");
            
            $info = DbInterface::getInfoFromDatabase($dbId);
            
            $view = new View();
            
            $headContentTemplate = new Template("$root/php/html_templates/head_content.php");
            $headContentTemplate->title = $title;
            echo $headContentTemplate;
        ?>
    </head>
    <body id="body-info" class="body-background">
        <?php
            $infoBodyTemplate = new Template("$root/php/html_templates/info_body.php");
            $infoBodyTemplate->title = $title;
            $infoBodyTemplate->view = $view;
            $infoBodyTemplate->info = $info;
            echo $infoBodyTemplate;
        ?>
    </body>
</html>

