<?php
    class Template
    {
        private $file = null;
        private $data = null;

        function __construct($file)
        {
            $this->file = $file;
            
            // __set will put these in the data array to be extracted into the 
            // current symbol table and consumed by the included
            // template file
            $this->title = "";
            
            $this->homeActive = false;
            $this->aboutActive = false;
            $this->allProductsActive = false;
            $this->boxPlenumActive = false;
            $this->retrofitPlenumActive = false;
            $this->contactActive = false;
            
            $this->contactFormSubHeader = "";
        }

        public function __set($key, $value)
        {
            $this->data[$key] = $value;
        }

        public function __get($key)
        {
            return $this->data[$key];
        }

        public function __toString()
        {
            extract($this->data);
            ob_start();
            include($this->file);
            $contents = ob_get_contents();
            ob_end_clean();
            return $contents;
        }
    }
?>
