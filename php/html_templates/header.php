<nav id="main-navbar" class="navbar navbar-default navbar-fixed-top">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>

            <a id="grisley-logo-container" class="pull-left flexbox-center-vertically" href="/index.php">
                <img id="grisley-logo-navbar" alt="Grisley" src="/img/grisley_logo.png">
            </a>
        </div>

        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li <?php if($homeActive){ echo "class='active'"; } ?>><a href="/index.php">Home</a></li>
                <li <?php if($aboutActive){ echo "class='active'"; } ?>><a href="/about.php">About</a></li>
                <li <?php if($projectsActive){ echo "class='active'"; } ?>><a href="/projects.php">Projects</a></li>
                <li class="dropdown<?php if($allProductsActive || $boxPlenumActive || $retrofitPlenumActive || $loadersActive){ echo " active"; } ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Products <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <!-- <li <?php if($allProductsActive){ echo "class='active'"; } ?>><a href="/products/products.php">All Products</a></li>
                        <li role="separator" class="divider"></li>-->
                        <li <?php if($boxPlenumActive){ echo "class='active'"; } ?>><a href="/products/box_plenum.php">Box Plenum</a></li>
                        <li <?php if($retrofitPlenumActive){ echo "class='active'"; } ?>><a href="/products/retrofit_plenum.php">Retrofit Plenum</a></li>
                        <li <?php if($agPlenumActive){ echo "class='active'"; } ?>><a href="/products/ag_plenum.php">AG Plenum</a></li>
                        <li <?php if($loadersActive){ echo "class='active'"; } ?>><a href="/products/loaders.php">Loaders & Transfers</a></li>
                    </ul>
                </li>
                <li class="dropdown<?php if($draftingDesignActive){ echo " active"; } ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li <?php if($draftingDesignActive){ echo "class='active'"; } ?>><a href="/services/drafting-design.php">Drafting & Design</a></li>
                    </ul>
                </li>
                <!-- <li <?php if($contactActive){ echo "class='active'"; } ?>><a href="/contact.php">Contact</a></li> -->
            </ul>
        </div>
    </div>
</nav>

