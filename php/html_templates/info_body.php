<?php
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    
    $headerTemplate = new Template("$root/php/html_templates/header.php");
    echo $headerTemplate;
?>

<div class="container-fluid">
    <div class="row">
        <div id="info-tab-wrapper" class="col-xs-12 col-md-offset-1 col-md-10 col-lg-offset-3 col-lg-6">
            <h3><?php echo $title ?></h3>
            <ul class="nav nav-tabs">
                <?php
                    echo $view->writeNavTabs($info->categories);
                ?>
            </ul>

            <div class="tab-content">
                <?php
                    echo $view->writeTabContent($info->documents);
                ?>
            </div>
        </div>
    </div>

</div>

<?php
    $footerTemplate = new Template("$root/php/html_templates/footer.php");
    echo $footerTemplate;
?>

