<footer class="col-md-12"> 
    <div class="container-fluid">
        <div id="footer-contact">
            <div class="col-md-offset-2 col-md-3">
                <a id="footer-contact-link-map" href="#">
                    <i class="fa fa-map-marker fa-2x"></i>
                    <span>Salt Lake City, Utah<br>Denver, Colorado</span>
                </a>
            </div>
            <div class="col-md-2">
                <a id="footer-contact-link-mail" href="mailto:info@grisley.com">
                    <i class="fa fa-envelope fa-2x"></i>
                    <span>info@grisley.com</span>
                </a>
            </div>
            <div class="col-md-2">
                <a href="tel:13037566474">
                    <i class="fa fa-phone-square fa-2x"></i>
                    <span>+1 303-756-6474</span>
                </a>
            </div>
        </div>
        <div id="subfooter" class="flexbox-center-both">
            <div class="flexbox-center-both">
                <p id="copyright">&copy; <span id="copyright-year"></span> Grisley ASC</p>

            </div>
        </div>
    </div>
</footer>

