<form role="form" id="contact-form" method="post" action="/php/send_mail.php">
    <h3>Send us a message</h3>
    <p><?php echo $contactFormSubHeader ?></p>
    <div class="row">
        <div class="form-group has-feedback col-md-6">
            <div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                <input name="name" placeholder="Name" class="form-control" type="text" autocorrect="off" required data-error="Please enter your name">
            </div>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>
        </div>
        <div class="form-group has-feedback col-md-6">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                <input name="email" placeholder="E-mail address" class="form-control" type="email" autocorrect="off" autocapitalize="off" required data-error="Please enter a valid e-mail address">
            </div>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="row">
        <div class="form-group has-feedback col-xs-12">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                <input name="subject" placeholder="Subject" class="form-control" type="text" required data-error="Please enter a subject">
            </div>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="row">
        <div class="form-group has-feedback col-xs-12">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                <textarea class="form-control" name="message" placeholder="Write your message here..." required data-error="Please enter a message" rows="9"></textarea>
            </div>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-7 feedback-message-wrapper">
            <i class="fa fa-spinner fa-spin"></i>
            <p class="feedback-message"></p>
        </div>
        <div class="form-group col-md-5 submit-button-wrapper">
            <button type="submit" class="btn btn-lg btn-success hidden-xs">Send Message <span class="glyphicon glyphicon-send"></span></button>
            <button type="submit" class="btn btn-success hidden-sm hidden-md hidden-lg">Send Message <span class="glyphicon glyphicon-send"></span></button>
        </div>
    </div>
</form>

