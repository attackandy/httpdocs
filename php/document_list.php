<?php
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    require_once("$root/php/document.php");  
    
    class DocumentList
    {
        public $categoryId = -1;
        public $categoryName = "";
        public $documents = array();
        
        function __construct($categoryId, $categoryName)
        {
            $this->categoryId = $categoryId;
            $this->categoryName = $categoryName;
        }
        
        public function addDocument($id, $path)
        {
            $document = new Document($id, $path);
            array_push($this->documents, $document);
        }
    }
?>