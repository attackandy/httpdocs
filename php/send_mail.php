<?php

function canonicalize(&$params) {
    $params['email'] = filter_var($params['email'], FILTER_SANITIZE_EMAIL);
}

function validate(&$params) {
    $errors = array();
    
    if (empty($params['name'])) {
        $errors['name'] = true;
    }
    if (empty($params['email']) || !filter_var($params['email'], FILTER_VALIDATE_EMAIL)) {
        $errors['email'] = true;
    }
    if (empty($params['subject'])) {
        $errors['subject'] = true;
    }
    if (empty($params['message'])) {
        $errors['message'] = true;
    }
    return $errors;
}

function recipient() {
    return "info@grisley.com";
}

function subject(&$params) {
    return mb_encode_mimeheader("New message from " . $params['name'], 'UTF-8', 'Q');
}

function clean_string($string) {

    $bad = array("content-type","bcc:","to:","cc:","href");

    return str_replace($bad,"",$string);

}

function body(&$params) {
    // Don't bother escaping e-mail body; it's for human consumption.
    return sprintf(
        "Name: %s\n" .
        "Email: %s\n" .
        "Subject: %s\n" .
        "Message:\n%s\n",
        $params['name'], $params['email'],
        $params['subject'], $params['message']);
}

function headers(&$params) {
    return sprintf(
        "From: %s <%s>",
        mb_encode_mimeheader($params['name'], 'UTF-8', 'Q'),
        $params['email']
    );
}

//Only process POST reqeusts.
if ($_SERVER["REQUEST_METHOD"] != "POST") {
    // Not a POST request, set a 405 (Method Not Allowed) response code.
    //http_response_code(405);
    header('X-PHP-Response-Code: 405', true, 405);
    echo "There was a problem with your submission, please try again.";
} 
else {
    $params = filter_input_array(INPUT_POST);
    
    canonicalize($params);

    if (($errors = validate($params))) {
        //http_response_code(400);
        header('X-PHP-Response-Code: 400', true, 400);
        echo json_encode($errors);
    } elseif (!mail(recipient($params), subject($params), body($params), headers($params))) {
        echo "Oops! Something went wrong and we couldn't send your message.";
    } else {
        echo 'Thank you! Your message has been sent and we will be contacting you shortly with a response.';
    }
}

//require '../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

//Only process POST reqeusts.
//if ($_SERVER["REQUEST_METHOD"] != "POST") {
//    // Not a POST request, set a 405 (Method Not Allowed) response code.
//    //http_response_code(405);
//    header('X-PHP-Response-Code: 405', true, 405);
//    echo "There was a problem with your submission, please try again.";
//} 
//else {
//    $params = filter_input_array(INPUT_POST);
//    
//    canonicalize($params);
//
//    if (($errors = validate($params))) {
//        //http_response_code(400);
//        header('X-PHP-Response-Code: 400', true, 400);
//        echo json_encode($errors);
//    }
//    else {
//        //Create a new PHPMailer instance
//        $mail = new PHPMailer;
//        // Set PHPMailer to use the sendmail transport
//        $mail->isSendmail();
//        //Set who the message is to be sent from
//        $mail->setFrom(clean_string($params['email']));
//        //Set an alternative reply-to address
//        //$mail->addReplyTo('replyto@example.com', 'First Last');
//        //Set who the message is to be sent to
//        $mail->addAddress('paul.r.fake@gmail.com');
//        //Set the subject line
//        $mail->Subject = $params['subject'];
//        //Read an HTML message body from an external file, convert referenced images to embedded,
//        //convert HTML into a basic plain-text alternative body
//        //$mail->msgHTML(body($params));
//        //Replace the plain text body with one created manually
//        $mail->AltBody = body($params);
//        //Attach an image file
//        //$mail->addAttachment('images/phpmailer_mini.png');
//        //send the message, check for errors
//        if (!$mail->send()) {
//            echo "Mailer Error: " . $mail->ErrorInfo;
//        } else {
//            echo 'Thank you! Your message has been sent and we will be contacting you shortly with a response.';
//        }
//    }
//}

?>