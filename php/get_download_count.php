<?php
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    require_once("$root/php/db_interface.php");
    require_once("$root/php/view.php");
    
    $item = filter_input(INPUT_POST, 'item');
    $sinceTimestamp = filter_input(INPUT_POST, 'sinceTimestamp');

    echo DbInterface::getDownloadCount($item, $sinceTimestamp);
?>

