<?php
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    require_once("$root/php/db_interface.php");
    require_once("$root/php/view.php");
    
    $categoryName = filter_input(INPUT_POST, 'name');
    $categoryId = filter_input(INPUT_POST, 'id');
    $dbId = filter_input(INPUT_POST, 'dbId');
    
    $categories = DbInterface::renameCategory($categoryId, $categoryName, $dbId);
    
    $view = new View();
    $categoryOptions = $view->writeCategoryOptions($categories);
    $categoriesToEdit = $view->writeCategoriesToEdit($categories, $dbId);
    $arr = array("categoryOptions" => $categoryOptions,
        "categoriesToEdit" => $categoriesToEdit);

    echo json_encode($arr);
?>

