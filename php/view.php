<?php
    class View
    {       
        public function writeNavTabs($categories)
        {
            $first = true;
            $output = "";
            foreach($categories as $category)
            {
                if ($first)
                {
                    $output .= "<li class='active'>";
                }
                else
                {
                    $output .= "<li>";
                }
                
                $categoryId = $category->id;
                $output .= "<a data-toggle='tab' href='#$categoryId'>";
                
                $output .= $category->name;
                
                $output .= "</a></li>";
                
                $first = false;
            }
            
            return $output;
        }
        
        public function writeTabContent($documents)
        {
            $first = true;
            $output = "";
            foreach($documents as $documentList)
            {
                $categoryId = $documentList->categoryId;
                $output .= "<div id='$categoryId' ";
                
                $output .= "class='tab-pane";
                if ($first)
                {
                    $output .= " active";
                }
                $output .= "'>";
                
                foreach($documentList->documents as $document)
                {
                    $path = $document->path;
                    $name = $document->name;
                    $output .= "<div class='info-item'><a href='$path'>$name</a></div>";
                }
                
                $output .= "</div>";
                
                $first = false;
            }
            
            return $output;
        }
        
        public function writeCategoryOptions($categories)
        {
            $output = "";
            foreach($categories as $category)
            {
                $categoryId = $category->id;
                $output .= "<option value='$categoryId'>";

                $output .= "$category->name</option>";
            }
            
            return $output;
        }
        
        public function writeCategoriesToEdit($categories, $dbId)
        {
            $output = "";
            foreach($categories as $category)
            {
                $categoryName = $category->name;
                $output .= "<p data-name='$categoryName'>$categoryName";
                $output .= "&nbsp;&nbsp;&nbsp;";
                
                $categoryId = $category->id;
                $output .= "<a id='rename-category-$categoryId' href='#' onclick='OnCategoryRenameClick(this.id, $dbId)'>rename</a>";
                
                $output .= "&nbsp;&nbsp;";
                $output .= "<a id='delete-category-$categoryId' href='#' onclick='OnCategoryDeleteClick(this.id, $dbId)'>delete</a></p>";
            }
            
            return $output;
        }
        
        public function writeFilesToDelete($documents)
        {
            $output = "";
            foreach($documents as $documentList)
            {
                $categoryName = $documentList->categoryName;

                foreach($documentList->documents as $document)
                {
                    $id = $document->id;
                    
                    $output .= "<tr>";
                    $output .= "<td><input type='checkbox' id='delete-file-$id' /></td>";
                    $output .= "<td>$id</td>";
                    $output .= "<td>$document->name</td>";
                    $output .= "<td>$categoryName</td>";
                    $output .= "<td>$document->path</td>";
                    $output .= "</tr>";
                }
            }
            
            return $output;
        }
    }
?>

