<?php
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    require_once("$root/php/db_interface.php");
    require_once("$root/php/view.php");
    
    $json = filter_input(INPUT_POST, 'ids');
    $dbId = filter_input(INPUT_POST, 'dbId');
    
    $documentIds = json_decode($json, true);
    
    $documents = DbInterface::deleteDocuments($documentIds, $dbId);
    
    $view = new View();
    echo $view->writeFilesToDelete($documents);
?>
